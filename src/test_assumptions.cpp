#include <stdio.h>
#include "samtools/sam.h"

using namespace std;

/*
#define BAM_FPAIRED        1 // template having multiple segments in sequencing
#define BAM_FPROPER_PAIR   2 // each segment properly aligned according to the aligner
#define BAM_FUNMAP         4 // segment unmapped
#define BAM_FMUNMAP        8 // next segment in the template unmapped
#define BAM_FREVERSE      16 // SEQ being reverse complemented
#define BAM_FMREVERSE     32 // SEQ of the next segment in the template being reversed
#define BAM_FREAD1        64 // the first segment in the template
#define BAM_FREAD2       128 // the last segment in the template
#define BAM_FSECONDARY   256 // secondary alignment
#define BAM_FQCFAIL      512 // not passing quality controls
#define BAM_FDUP        1024 // PCR or optical duplicate
#define BAM_FSUPPLEMENTARY 2048 // supplementary alignment
*/

// assumptions
static bool a1 = true;
static bool a2 = true;

// Callback for bam_fetch().
static int fetch_func(const bam1_t *b, void *)
{
  uint32_t flag = b->core.flag;
  a1 = a1 && (flag & BAM_FPROPER_PAIR);
  if (flag & BAM_FPROPER_PAIR) {
    a2 = a2 && ((flag & BAM_FREAD1) ^ (flag & BAM_FREAD2));
  }
  return 0;
}


int main(int argc, char* argv[])
{
  int begin, end;
  samfile_t* in;
  int ref;
  bam_index_t* idx;

  if (argc <= 2) {
    fprintf(stderr, "Usage: %s <in.bam> <region>\n", argv[0]);
    return 1;
  }

  in = samopen(argv[1], "rb", 0);
  if (in == 0) {
    fprintf(stderr, "Fail to open BAM file %s\n", argv[1]);
    return 1;
  }

  idx = bam_index_load(argv[1]); // load BAM index
  if (idx == 0) {
    fprintf(stderr, "BAM indexing file is not available.\n");
    return 1;
  }

  bam_parse_region(in->header, argv[2], &ref, &begin, &end); // parse the region
  if (ref < 0) {
    fprintf(stderr, "Invalid region %s\n", argv[2]);
    return 1;
  }

  bam_fetch(in->x.bam, idx, ref, begin, end, 0, fetch_func);
  bam_index_destroy(idx);
  
  printf("%d\n", a1);
  printf("%d\n", a2);

  samclose(in);
  return 0;
}
