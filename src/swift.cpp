#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include "swift.h"
#include "sv.h"
#include "clust/anch.h"
#include "clust/libswan_clust.h"
#include "sclip/libswan_sclip.h"
#include "api/BamReader.h"
#include "remap/vcf.h"
//#include <boost/unordered_map.hpp>
//#include "remap/delly.h"
//#include "remap/somatic_filter.h"

#define max_chr_len 3e8

using namespace std;

//IMPROVE:
//sv need to allow statistical merging any of the two evidence
//sv need to allow including remapping and assembling result
//sv need to allow write to standard vcf or bed format
//sv functions 

struct MasterConfig {
	string region;
  string svType;
	string casefiles;
	string basefiles;
	vector<boost::filesystem::path> files; // bam files
	boost::filesystem::path vcfFile;
	boost::filesystem::path outfile;
	boost::filesystem::path bedpefile;
  
};

struct SclipConfig {
  string chromosomeName;
  int minReadPerCluster;
  int minBasePerCluster;
  int scanStart;
  int scanEnd;
  int trunkSize;
  string spout;
	vector<boost::filesystem::path> files;
};

template<typename T>
static T convert(const string& s) {
  T r;
  istringstream iss(s);
  iss >> r;
  return r;
}

static bool readVCF(const string& filename, vector<SV>& swiftSVs, const BamTools::RefVector& references) {
  // create a dictionary that maps chromosome to the absolute start position of that chromosome
  map<string, int64_t> chrStarts;
  int64_t totalLength = 0;
  BamTools::RefVector::const_iterator itRef = references.begin();
  for (unsigned int i = 0; itRef != references.end(); ++itRef, ++i) {
    chrStarts[itRef->RefName] = totalLength;
    totalLength += itRef->RefLength;
  }

  // read the VCF file
  vcf::VariantCallFile variantFile;
  variantFile.open(filename);
  if (!variantFile.is_open()) {
    fprintf(stderr, "Can't open the VCF file\n");
    return false;
  }

  vcf::Variant record(variantFile);
  while (variantFile.getNextVariant(record)) {
    if (record.info.find("SVTYPE") == record.info.end())
      continue;

    int64_t chromosome_start = chrStarts[record.chrom];
    int64_t st = chromosome_start + record.position - 1;
    int64_t ed = chromosome_start + convert<int>(record.info["END"]) - 1;

    bool st_fwd, ed_fwd;
    string svType = record.info["SVTYPE"];
    if (svType == "DEL") {
      st_fwd = true;
      ed_fwd = false;
    } else if (svType == "INS") {
      // TODO: not sure
      st_fwd = true;
      ed_fwd = false;
    } else if (svType == "INV") {
      // TODO: not sure
      st_fwd = true;
      ed_fwd = true;
    } else {
      // TODO: what is IGN and MSK ?
      continue;
      // TODO: not sure
      st_fwd = true;
      ed_fwd = false;
    }
    
    SV sv;
    sv.st = st_fwd ? st : (totalLength + st);
    sv.ed = ed_fwd ? ed : (totalLength + ed);
    sv.st_ci = pair<int, int>(-10, 10);
    sv.ed_ci = pair<int, int>(-10, 10);
    swiftSVs.push_back(sv);
  }
  return true;
}

int main(int argc, char **argv) {
  MasterConfig c;
	vector<rdp> anchors;
  vector<SV> svs;

  // Define generic options
  boost::program_options::options_description generic("Generic options");
  generic.add_options()
    ("help,?", "show help message")
    ("type,t", boost::program_options::value<string>(&c.svType)->default_value("ALL"), "SV analysis type (DEL, DUP, INV, TRA)")
    ("vcffile,v", boost::program_options::value<boost::filesystem::path>(&c.vcfFile)->default_value(""), "VCF file")
    ("outfile,o", boost::program_options::value<boost::filesystem::path>(&c.outfile)->default_value("sv.vcf"), "SV output file")
    ("region,r", boost::program_options::value<string>(&c.region)->default_value(""), "region to scan, e.g.: chr1 | chr1:500 | chr1:500..1000 | chr1:500..chr3:750 .")
    ("casefiles,c", boost::program_options::value<string>(&c.casefiles)->required(), "input bam files from case population (at least one required)")
    ("basefiles,b", boost::program_options::value<string>(&c.basefiles), "input bam files from baseline population")
    ("bedpefile,p", boost::program_options::value<boost::filesystem::path>(&c.bedpefile)->default_value("clust.bedpe"), "clust output file (BEDPE)")
    ;
  boost::program_options::positional_options_description pos_args;

  // Set the visibility
  boost::program_options::options_description cmdline_options;
  cmdline_options.add(generic);
  boost::program_options::options_description visible_options;
  visible_options.add(generic);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).positional(pos_args).run(), vm); //stores all options
  
  // Check command line arguments
  if (vm.count("help")) {
    cout << "Usage: " << argv[0] << " [OPTIONS] -c <case1>[,case2,...] -b [base1,base2,...] " << endl;
    cout << visible_options << "\n";
    return 1;
  }

  boost::program_options::notify(vm); //raises option error
  
  // Convert comma separated paths to vector of strings
  vector<string> basefiles;
  vector<string> casefiles;
	vector<string> chr_svec; 
	vector<ul> chr_size_uvec;
	map<pair<bool,string>,ull> fwdchr_to_offset;
	map<ull,pair<bool,string> > offset_to_fwdchr;
	vector<ul> offset_keys;
  BamTools::RefVector references;
	string scanRegion(c.region);
	boost::filesystem::path bedpefile(c.bedpefile);
  
	typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
	boost::char_separator<char> sep(",");
	tokenizer tok(c.basefiles, sep);
	cerr<<"casefiles=";
  for(tokenizer::iterator beg=tok.begin(); beg!=tok.end(); ++beg){
		basefiles.push_back(*beg);
		if(beg!=tok.begin()) cerr<<",";
		cerr<<(*beg);
	}
	cerr<<endl;
	
	cerr<<"basefiles=";
	tok.assign(c.casefiles, sep);
  for(tokenizer::iterator beg=tok.begin(); beg!=tok.end(); ++beg){
		if(beg!=tok.begin()) cerr<<",";
		casefiles.push_back(*beg);
		cerr<<(*beg);
	}
	cerr<<endl;
	//NOTE: order in bedpe output is casefile1,casefile2,...,basefile1,basefile2,... 

	vector<string> files; 
	files.insert(files.begin(),casefiles.begin(),casefiles.end()); 
	files.insert(files.begin(),basefiles.begin(),basefiles.end());
	msi fn2fi; msd bstat; for(ull i=0; i<files.size(); ++i) fn2fi[files[i]] = i;

	//IMPROVE: reasonable pairwise whole genome analysis within a day and 40GB?
  //IMPROVE: for population analysis we need to enable fast Coverage access through a .cov file by google hash?
  /*
	map<string, unordered_map<ull,int> > filesCov;
	for(vector<string>::const_iterator vit=files.begin(); vit != files.end(); ++vit) {
		region_to_coverage((*vit), scanRegion, filesCov[(*vit)]); //create coverage map for each bams of scanned Region
	}
	*/

	//
	bstat["flank"] = 200;
  do_anchor_clust(files, fn2fi, bstat, anchors, scanRegion, chr_svec, chr_size_uvec, fwdchr_to_offset, offset_to_fwdchr, offset_keys); //anchors returned

	// ---------- OUTPUT TO BEDPE -------------
	map<string,vector<int> > counts;       //if size==0 means disable counts
	map<string,vector<int> > coverage;     //if size==0 means disable coverage
	//vector<string> notes();
	for(vector<string>::const_iterator vit=files.begin(); vit != files.end(); ++vit){
		counts[*vit] = vector<int>(anchors.size(),-1);
		coverage[*vit] = vector<int>(anchors.size(),-1);
	}
	vector<string> scores(anchors.size(),"");
	//TODO: write statistical routines to assign scores

	//WHAT WE NEED HERE?
  //  ABILITY TO SAVE TIME BY TRIMMING NON-ESSENTIAL OUTPUTS
  //  OUTPUTS SHOULD BE ABLE TO SUPPORT EXTERNAL STATISTICAL ANALYSIS
  //  DATA SHOULD BE ABLE TO SUPPORT INTERNAL STATISTICAL ANALYSIS WHILE COMPACT 
  //SOLUTION:
  //  notes will only be visible in anchor_to_bedpe and for external use
  //  coverage, scores, counts will be visible and retain contents for internal use 
  //  anchor_to_bedpe will generate all necessary bits, but can turn off notes and output with no bedpefile
  //  anchor_to_bedpe(anchors, coverage, scores, counts, bedpefile=NULL, offset_to_fwdchr, offset_keys);
  //TODO:
  //  new anchor_to_info, which will fill in coverage and counts [iteration similar to bam_to_note]
  //  new anchor_to_score, which will fill in scores [iteration similar to anchor_to_bedpe]

	anchor_to_counts(anchors, files, bstat, counts);

	anchor_to_coverage(anchors, files, bstat, coverage, offset_to_fwdchr, offset_keys);

	//anchor_to_score(anchors, casefiles, basefiles, coverage, counts, scores, statfunc);

  anchor_to_bedpe(anchors, files, coverage, scores, counts, bedpefile, offset_to_fwdchr, offset_keys);

  // ---------- RUN SCLIP ----------
  /*
  SclipConfig c;

  // Define generic options
  boost::program_options::options_description generic("Generic options");
  generic.add_options()
    ("help,?", "show help message")
    ("chromosomeName,c", boost::program_options::value<string>(&c.chromosomeName)->default_value("11"), "chromosome to scan")
    ("minReadPerCluster,i", boost::program_options::value<int>(&c.minReadPerCluster)->default_value(3), "minimal number of reads per cluster")
    ("minBasePerCluster,j", boost::program_options::value<int>(&c.minBasePerCluster)->default_value(30), "minimal number of total bases per cluster")
    ("scanStart,u", boost::program_options::value<int>(&c.scanStart)->default_value(1), "1-indexed scan start")
    ("scanEnd,v", boost::program_options::value<int>(&c.scanEnd)->default_value(max_chr_len), "1-indexed scan end")
    ("trunkSize,z", boost::program_options::value<int>(&c.trunkSize)->default_value(1000000), "trunk size for scanning bamfile")
    ("spout,o", boost::program_options::value<string>(&c.spout)->default_value("input"), "sample output prefix")
    ;

  // Define hidden options
  boost::program_options::options_description hidden("Hidden options");
  hidden.add_options()
    ("input-file", boost::program_options::value< vector<boost::filesystem::path> >(&c.files), "input file")
    ;

  pos_args.add("input-file", -1);
  
  // Set the visibility
  boost::program_options::options_description cmdline_options;
  cmdline_options.add(generic).add(hidden);
  boost::program_options::options_description visible_options;
  visible_options.add(generic);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).positional(pos_args).run(), vm);
  boost::program_options::notify(vm);
  
  // Check command line arguments
  if ((vm.count("help")) || (!vm.count("input-file"))) {
    // TODO: fix
    cout << "Usage: " << argv[0] << " [OPTIONS] <sample1.bam> <sample2.bam> ..." << endl;
    cout << visible_options << "\n";
    return 1;
  }
  
  // Show cmd
  for(int i=0; i<argc; ++i) { cout << argv[i] << ' '; }
  cout << endl;
  */
  
  
  /*
  int n_trunks = ceil((double)(c.scanEnd - c.scanStart + 1) / c.trunkSize);
  double SC_PROPORTION_MISMATCH_THRESH = 0.05;
  int MIN_GAP_DISTANCE = 10000;
  

  for (int ti = 1; ti <= n_trunks; ++ti) {
    cpp_core_sclip(ti, n_trunks, c.scanStart, c.scanEnd, c.trunkSize, files, c.chromosomeName,
      c.minReadPerCluster, c.minBasePerCluster, SC_PROPORTION_MISMATCH_THRESH, MIN_GAP_DISTANCE);
  }
  */



  // ---------- RUN DELLY ----------
  /*
  vector<SV> swiftSVs;
  
  if (c.vcfFile.string() != "") {
    // Read the VCF file
    BamTools::BamReader reader;
    reader.Open(files[0]);
    BamTools::RefVector references = reader.GetReferenceData();
  
    readVCF(c.vcfFile.string(), swiftSVs, references);
  }
 

  DellyConfig dellyConfig;
  dellyConfig.svType = c.svType;
  dellyConfig.files = c.files;
  dellyConfig.outfile = c.outfile;
  vector<string> svTypes;
  if (dellyConfig.svType == "ALL") {
    svTypes.push_back("DEL");
    svTypes.push_back("DUP");
    svTypes.push_back("INV");
    svTypes.push_back("TRA");
    // TODO: fix INS
    //svTypes.push_back("INS");
  } else {
    svTypes.push_back(dellyConfig.svType);
  }
  string realOutfile = dellyConfig.outfile.string();

  for (size_t i = 0; i < svTypes.size(); ++i) {
    const string& svType = svTypes[i];
    string out1 = realOutfile + "." + svType + ".vcf";
    string out2 = realOutfile + "." + svType + ".somatic.vcf";
    
    cout << "Run delly for " << svType << endl << endl;
    dellyConfig.svType = svType;
    dellyConfig.outfile = out1;
    if (run_delly(dellyConfig, swiftSVs) != 0) {
      cerr << "An error has occurred" << endl;
      return 1;
    }
    
    cout << "Run somaticFilter for " << svType << endl << endl;
    if (run_somatic_filter(out1, out2, svType) != 0) {
      cerr << "An error has occurred" << endl;
      return 1;
    }
  }

  cout << "Merge VCF files" << endl;
  ofstream out(realOutfile.c_str());
  for (size_t i = 0; i < svTypes.size(); ++i) {
    const string& svType = svTypes[i];
    string out2 = realOutfile + "." + svType + ".somatic.vcf";
    ifstream file(out2.c_str());
    if (i > 0) { // skip the header
      string line;
      while (getline(file, line)) {
        if (!line.empty() && line[0] != '#') {
          out << line << endl;
          break;
        }
      }
    }
    out << file.rdbuf();
    file.close();
  }
  out.close();
  */

  return 0;
}
