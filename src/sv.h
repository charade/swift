#ifndef SV_H
#define SV_H

#include <string>
#include <vector>
#include <stdint.h>

/* sv interface */
class SV { //internal coordinate system will use chr+pos converted to int
public:
	int64_t st; // 0-based start, internal use encoding chr, pos and strand; st=sum(len_of_all_chr)*(strand==-)+(sum_of_upto_chr)+pos; 
	int64_t ed; // 0-based end, internal use encoding chr, pos, and strand
	int type; // type: 1=DEL, 2=INS, 3=DUP, 4=INV, 5=TRP
	std::pair<int,int> st_ci; // confidence interval relative bases (-10,10)
	std::pair<int,int> ed_ci; // confidence interval relative bases (-10,10)
	int cvs; //scale for evidence if ev=cv in reporting coverage 
	int ev; //0=remap, 1=assemble, 2=cl, 3=rp, 4=cv
	std::vector<int> dp_st_ctrl; //read depth of st_ci
	std::vector<int> dp_st_samp; //read depth of st_ci
	std::vector<int> dp_ed_ctrl; //read depth of ed_ci
	std::vector<int> dp_ed_samp; //read depth of ed_ci
	// some may not have this, shall we keep all of them
	std::vector<SV> evi[5]; // store latest merged evidence for this sv, 
	// do we need to create more evidence fields
	// evi[0] for remap, evi[1] for assemble, evi[2] for clip, evi[3] for read pair, evi[4] for coverage, 
	// note for cv we only need ev_st_ctrl and ev_st_samp which saves [st,ed] coverage information in cvs 
	std::vector<int> ev_st_ctrl; //# of cl,rp support sv at st_ci or cv between (st,ed) or remapped reads or assembled reads 
	std::vector<int> ev_st_samp; //# of cl,rp support sv at st_ci or cv between (st,ed) or remapped reads or assembled reads
	std::vector<int> ev_ed_ctrl; //# of cl,rp support sv at ed_ci or cv between (st,ed) or remapped reads or assembled reads
	std::vector<int> ev_ed_samp; //# of cl,rp support sv at ed_ci or cv between (st,ed) or remapped reads or assembled reads
	int sv_from_cl(); // create sv from clipping events
	int sv_from_rp(); // create sv from read pair events
	int sv_from_cv(); // create sv from coverage events
	int sv_merge(SV v); // merge evidence of possibly duplicated events, from different type of evidence, just insert to empty slots in evi
	//but if merging from the same type of evidence, need to, what shall we do? 
};

#endif
