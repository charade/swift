//TODO: need an easy way to calculate and store coverage
//ROADMAP: DONE 1. keep cluster data in rdp data structure without statistical analysis (BEDPE)
//         2. make a list of all positions into visitors and get local coverage for both ends
//         3. do either the fisher's exact test or population based calculation
//         4. genotype with sclip and delly

#ifndef LIBSWAN_CLUST_H
#define LIBSWAN_CLUST_H

#include <algorithm>
#include <limits>
#include <utility>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <api/BamReader.h>
#include <ctime>
//#include <boost/unordered_map.hpp>

using namespace std;

//using ba = BamTools::BamAlignment;
typedef unsigned long ul;				  //shorthand for unsigned long (32bit)
typedef unsigned long long ull;				 //shorthand for unsinged long long (64bit)
typedef map<string,ull> msi;
typedef map<string,double> msd;
typedef pair<ull,ull> rd;              //rd = <st,ed>, data structure for read
typedef BamTools::BamAlignment ba;
typedef vector<ba> vba;
typedef unordered_map<ull,vba> mvba;
typedef pair<rd,rd> per;
typedef pair<per,mvba> rdp;
typedef pair<ull,pair<ull,bool> > evt; //event = <pos,<rdp_idx,in(true)>>, data structure for rdp in(true)/out on scan line
typedef vector<ull> clust;             //clust = {rdp1,rpd2,...}, data structure for holding clustered rdp
//typedef pair<pair<rd,rd>,ul> rdpe; //rdp = <<rd1, rd2>, data>, data structure for a read pair
//typedef pair<pair<rd,rd>,map<string,vector<BamTools::BamAlignment>>> rdp; //rdp = <<rd1, rd2>, data>, data structure for a read pair
//typedef vector<ba> align;
//typedef vector<typename BamTools::BamAlignment > align;
//typedef typename pair<pair<rd,rd>,map<string,align> > rdp;//rdp = <<rd1, rd2>, data>, data structure for a read pair

map<pair<bool,string>,ull> make_offset(const vector<string>& chr_svec, const vector<ul>& chr_size_uvec); 
map<ull,pair<bool,string> > make_reverse(const map<pair<bool,string>,ull>& fwdchr_to_offset);
bool anch_clust(vector<rdp>& rdp_vec, ull flank);
bool show_rdp ( rdp& r );

/*
void anch_clust(
  const vector<bool>& Lfwd_bvec, const vector<string>& Lchr_svec, const vector<ul>& Lst_uvec, const vector<ul>& Led_uvec,
  const vector<bool>& Rfwd_bvec, const vector<string>& Rchr_svec, const vector<ul>& Rst_uvec, const vector<ul>& Red_uvec,
  const vector<string>& chr_svec, const vector<ul>& chr_size_uvec);

bool anch_clust( 
  const vector<rdp>& rdp_vec,
  const vector<string>& chr_svec, const vector<ul>& chr_size_uvec);

void contrast_clust(
  const vector<bool>& xLfwd_bvec, const vector<string>& xLchr_svec, const vector<ul>& xLst_uvec, const vector<ul>& xLed_uvec, const vector<bool>& xRfwd_bvec, const vector<string>& xRchr_svec, const vector<ul>& xRst_uvec, const vector<ul>& xRed_uvec,
  const vector<bool>& yLfwd_bvec, const vector<string>& yLchr_svec, const vector<ul>& yLst_uvec, const vector<ul>& yLed_uvec, const vector<bool>& yRfwd_bvec, const vector<string>& yRchr_svec, const vector<ul>& yRst_uvec, const vector<ul>& yRed_uvec,
  const vector<string>& chr_svec, const vector<ul>& chr_size_uvec, int x_sup, int y_sup,
  vector<SV>& svs);
*/

#endif
