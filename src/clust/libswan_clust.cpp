#include "libswan_clust.h"

using namespace std;

	/* DEBUG RDP
	for(vector<rdp>::iterator vit=rdp_vec.begin(); vit!=rdp_vec.end(); ++vit) {
		show_rdp(*vit); cout<<endl;
	}
	exit(1);
	*/

bool compare_as_evt (const evt& x, const evt& y) { //return true if evt x precedes evt y on scan line
	//vector<BamTools::BamAlignment > v;
	//align v;
  ba b;
	vba v;
  mvba m;
	per r;
	rdp p;
	//evt v;
  return x.first < y.first;
}

bool overlap_both (const rdp& x, const rdp& y, ull flank) { //return true if both end of rdp x and rdp y overlap
  return  ( !(x.first.first.second < y.first.first.first-flank || y.first.first.second+flank < x.first.first.first)
         && !(x.first.second.second < y.first.second.first-flank || y.first.second.second+flank < x.first.second.first) );
}

vector<clust> scan_clust(const vector<evt>& evt_vec, const vector<rdp>& rdp_vec, bool (*ov)(const rdp&, const rdp&, ull), ull flank) {
	//evt_vec has to be sorted
	time_t start = time(0);
	cerr<<"scan_clust: ..."; 
  vector<clust> clust_vec; // found clust
  unordered_map<ull,ull> rdp_active; // rdp_idx to clust_idx; keep active rdp and its clust id
  ull clust_id;                 // current clust_id
  vector<ull> rdp_clust = vector<ull>(rdp_vec.size());  //clust_vec for rdp_vec
  for (ull i = 0; i<evt_vec.size(); ++i) { //iterate through sorted event
		
		/* DEBUG
		if( i % 1000 == 0 ) {
			time_t end1 = time(0);
			time = difftime(end1, start);
			cerr<<endl<<" processed "<<i<<" of "<<evt_vec.size()<<",est time"<<(time/float(i))*(evt_vec.size()-i);
		}
		*/

    //cout<<i<<":"<<evt_vec[i].first<<":"<<evt_vec[i].second.first<<":"<<evt_vec[i].second.second<<endl;
    if (!evt_vec[i].second.second) { //if evt.second is true, env is out
      rdp_active.erase(evt_vec[i].second.first); continue;
		}
    
    clust_id = clust_vec.size();     //else, env is in, assume a new cluster
    for (unordered_map<ull,ull>::iterator it = rdp_active.begin(); it!=rdp_active.end(); ++it) {
      if (ov(rdp_vec[evt_vec[i].second.first],rdp_vec[(*it).first],flank)) { 
        clust_id = (*it).second; break; 
      }
    }  //iterating through all active rdp and assign the new rdp the same clust id
       //if both ends overlap
    rdp_active[evt_vec[i].second.first]=clust_id; //rdp_active[rdp_idx]=clust_id; map
    rdp_clust[evt_vec[i].second.first]=clust_id; //rdp_clust[rdp_idx]=clust_id; vector
    if (clust_id == clust_vec.size()) { 
      clust_vec.push_back(clust(1,evt_vec[i].second.first)); }  //rdp is first in clust
    else { 
      clust_vec[clust_id].push_back(evt_vec[i].second.first); } //rdp is added to clust
  }

	time_t end = time(0); double time = difftime(end, start);
	cerr<<"done ("<<time<<"s)";
  return clust_vec;
} // given rdp and their sorted scan events and overlapping function return clusters that contain the overlapping rdp

bool show_clust(const clust& c, vector<rdp> rv){ 
	cout<<"show_clust: <"<<c.size()<<">:: (";
  for (clust::const_iterator it = c.begin(); it!=c.end(); ++it) {
		show_rdp(*(rv.begin()+(*it)));  //WHY: this requires rv to pass by value, fail if use rv is reference
		if(it-c.end()!=1) cout<<",";
	}
	cout<<")";
	return true;
}

rdp collapse_clust(const clust& clust_item, const vector<rdp>& rdp_vec) {
	/* DEBUG
  cerr<<"collapse_clust: collapsing rdps into one ...";
	show_clust(clust_item, rdp_vec); cout<<endl;
	*/
  ull Lst = numeric_limits<ull>::max(); ull Led = 0; 
  ull Rst = numeric_limits<ull>::max(); ull Red = 0; 
	mvba file2bam;  // map keep track of alignments associated with filename
  for (clust::const_iterator it = clust_item.begin(); it!=clust_item.end(); ++it) { //iter through cluster rdps
    if (rdp_vec[(*it)].first.first.first<Lst) Lst = rdp_vec[(*it)].first.first.first; 
    if (rdp_vec[(*it)].first.first.second>Led) Led = rdp_vec[(*it)].first.first.second; 
    if (rdp_vec[(*it)].first.second.first<Rst) Rst = rdp_vec[(*it)].first.second.first; 
    if (rdp_vec[(*it)].first.second.second>Red) Red = rdp_vec[(*it)].first.second.second; 
		for (mvba::const_iterator mit = rdp_vec[(*it)].second.begin(); mit != rdp_vec[(*it)].second.end(); ++mit) {
			//iter through bam files, mit=file:[r1,r2,...]
			if(file2bam.find(mit->first)!=file2bam.end()) {  //file key is in file2bam 
				file2bam[mit->first].insert(file2bam[mit->first].end(),mit->second.begin(),mit->second.end());
			} else { //file key is not in file2bam, use mit as initial value
			 file2bam[mit->first]=mit->second; 
			}
		} //this supposed merge all k-v pairs of all clustered rdps into file2bam 
  }
  rdp clap = make_pair(make_pair(make_pair(Lst,Led),make_pair(Rst,Red)),file2bam);
	/* DEBUG
	show_rdp(clap); cout<<endl;
  cerr<<"done."<<endl;
	*/
  return clap;
} // collapse a rdp clust into a meta rdp describing outfit and count

map<pair<bool,string>,ull> make_offset(const vector<string>& chr_svec, const vector<ul>& chr_size_uvec) {
  map<pair<bool,string>,ull> fwdchr_to_offset = map<pair<bool,string>,ull>();
  size_t n = chr_svec.size();
  size_t a = 0;
  size_t t = 0;
  for (size_t i = 0; i<n; ++i) { a += chr_size_uvec[i]; } 
  for (size_t i = 0; i<n; ++i) {
    fwdchr_to_offset[make_pair(true,chr_svec[i])] = t;
    fwdchr_to_offset[make_pair(false,chr_svec[i])] = a+t;
    t += chr_size_uvec[i];
  }
  return fwdchr_to_offset;
} // this creates the global coordinates for both fwd/bwd sequences of reference genome
  // the coordinate is basically connected as +chr1_+chr2_..._+chrY_-chr1_-chr2_..._-chrY

map<ull,pair<bool,string> > make_reverse(const map<pair<bool,string>,ull>& fwdchr_to_offset) {
  map<ull,pair<bool,string> > offset_to_fwdchr;
  map<pair<bool,string>,ull>::const_iterator mit;
  for (mit = fwdchr_to_offset.begin(); mit!=fwdchr_to_offset.end(); ++mit) {
     offset_to_fwdchr[mit->second] = mit->first; 
  }
  return offset_to_fwdchr;
}  // this creates the master table that convert convert global index back to (chr?, dir?, pos?)

vector<evt> coord_to_evt(const vector<rdp>& rdp_vec, ull flank=200ull) { //rdp enters pos-flank and exits pos+rl+flank
  size_t n = rdp_vec.size(); 
  vector<evt> evt_vec = vector<evt>(2*n);
  for (size_t i = 0; i<n; ++i) {
    evt_vec[2*i]=make_pair(rdp_vec[i].first.first.first-flank,make_pair(i,true)); // idx enter
    evt_vec[2*i+1]=make_pair(rdp_vec[i].first.first.second+flank,make_pair(i,false));
  }
  return evt_vec;
}  // this creates scan line events based on rdp_vec we have

bool show_rdp ( rdp& r ) {
  //"[(st1,ed1)-(st2,ed2)]->[f1:rn1,rn2,rn3;f2:rn1,rn2,rn3]
	cout<<"show_rdp: <"<<r.second.size()<<">::";
	cout<<"[("<<r.first.first.first<<",";
	cout<<r.first.first.second<<")";
	cout<<"-";
	cout<<"("<<r.first.second.first<<",";
	cout<<r.first.second.second<<")]";
	cout<<"->[";
	for(mvba::iterator m=r.second.begin(); m!=r.second.end(); ++m) {
		if(m!=r.second.begin()) cout<<";";
		cout<<(*m).first<<"{";
		for(vba::iterator v=(*m).second.begin(); v!=(*m).second.end(); ++v) {
			if(v!=(*m).second.begin()) {
				cout<<",";
			} 
			cout<<(*v).Name;
		}
		cout<<"}";
	}
	cout<<"]";
	return true;
}

bool anch_clust(vector<rdp>& rdp_vec, ull flank) {

	cerr<<"anch_clust: rdps before collapse "<<rdp_vec.size()<<endl;
	cerr<<"anch_clust: parameter flank= "<<flank<<endl;
	cerr<<"anch_clust: converting to scan line events ...";
  vector<evt> evt_vec = coord_to_evt(rdp_vec, flank);
	cerr<<"done."<<endl;
	cerr<<"anch_clust: sorting scan line events ...";
  stable_sort(evt_vec.begin(), evt_vec.end(), compare_as_evt);
	cerr<<"done."<<endl;
	cerr<<"anch_clust: clustering scan line events ...";
  vector<clust> clust_vec = scan_clust(evt_vec,rdp_vec,overlap_both,flank);
	cerr<<"done."<<endl;
  ull n = clust_vec.size();
  vector<rdp> clust_rdp_vec = vector<rdp>(n);

	/* DEBUG
	for(vector<rdp>::iterator vit=rdp_vec.begin(); vit!=rdp_vec.end(); ++vit) {
		show_rdp(*vit); cout<<endl;
	}
	exit(1);
	*/

  for (ull i = 0; i<n; ++i) {
    clust_rdp_vec[i] = collapse_clust(clust_vec[i],rdp_vec);
  }

	cerr<<"anch_clust: rdps after collapse ... "<<clust_rdp_vec.size();
	rdp_vec.clear(); //empty rdp_vec

	/* DEBUG
	for(vector<rdp>::iterator vit=clust_rdp_vec.begin(); vit!=clust_rdp_vec.end(); ++vit) {
		show_rdp(*vit); cout<<endl;
	}
	cerr<<rdp_vec.size()<<endl;
	cerr<<clust_rdp_vec.size()<<endl;
	*/

	for( vector<rdp>::iterator vit=clust_rdp_vec.begin(); vit!=clust_rdp_vec.end(); ++vit ) {
		rdp_vec.push_back(*vit);
	}
	// copy(clust_rdp_vec.begin(),clust_rdp_vec.end(),rdp_vec.begin()); 
	// assign is not allowed; copy failed "pointer being freed was not allocated"

	cerr<<"done"<<endl;
	return true;
}


/*
rdp collapse_contrast(const clust& clust_item, const vector<rdp>& rdp_vec, int x_sup, int y_sup) {
  ull Lst = numeric_limits<ull>::max(); ull Led = 0;
  ull Rst = numeric_limits<ull>::max(); ull Red = 0;
  int xs = 0; int ys = 0; //report cluster only if combined contrast >-y_sup, combined support >=x_sup 
  for (vector<ull>::const_iterator it = clust_item.begin(); it!=clust_item.end(); ++it) {
    if (rdp_vec[(*it)].second >0) { //test if is a positive supporting cluster, increasing tumor count
      xs += rdp_vec[(*it)].second; 
      if (rdp_vec[(*it)].first.first.first<Lst) Lst = rdp_vec[(*it)].first.first.first;
      if (rdp_vec[(*it)].first.first.second>Led) Led = rdp_vec[(*it)].first.first.second;
      if (rdp_vec[(*it)].first.second.first<Rst) Rst = rdp_vec[(*it)].first.second.first;
      if (rdp_vec[(*it)].first.second.second>Red) Red = rdp_vec[(*it)].first.second.second;
    } else { //if is a negative supporting cluster, increasing control count
      ys += rdp_vec[(*it)].second;
    }
  }
  rdp clap = make_pair(make_pair(make_pair(0,0),make_pair(0,0)),0); //a NULL result
  if (xs>=x_sup && !(ys<=y_sup))
    clap = make_pair(make_pair(make_pair(Lst,Led),make_pair(Rst,Red)),xs);
  return clap;
} // this is a over simplified screening function which basically decides based on thresh of x_sup and y_sup
*/


/*
vector<rdp> anch_to_coord(
    const vector<bool>& Lfwd_bvec, const vector<string>& Lchr_svec, const vector<ul>& Lst_uvec, const vector<ul>& Led_uvec,
    const vector<bool>& Rfwd_bvec, const vector<string>& Rchr_svec, const vector<ul>& Rst_uvec, const vector<ul>& Red_uvec,
    map<pair<bool,string>,ull>& fwdchr_to_offset, 
		const vector<vector<BamTools::BamAlignment> >& bamAl_vec) {
  size_t n = Lfwd_bvec.size();
  vector<rdp> rdp_vec = vector<rdp>(n);
  for (size_t i = 0; i<n; i++) {
    ull Loffset = fwdchr_to_offset[make_pair(Lfwd_bvec[i],Lchr_svec[i])];
    ull Lstart = Loffset+Lst_uvec[i];
    ull Lend = Loffset+Led_uvec[i];
    ull Roffset = fwdchr_to_offset[make_pair(Rfwd_bvec[i],Rchr_svec[i])];
    ull Rstart = Roffset+Rst_uvec[i];
    ull Rend = Roffset+Red_uvec[i];
    rdp_vec[i] =  (Rstart>=Lstart) ? make_pair(make_pair(make_pair(Lstart,Lend),make_pair(Rstart,Rend)),bamAl_vec[i]) : make_pair(make_pair(make_pair(Rstart,Rend),make_pair(Lstart,Lend)),bamAl_vec[i]); 
  }
  return rdp_vec;
}  // read in the anchor.txt file as vectors and convert them to rdp_vec 
	 // TODO: this is actually we need to feed in from what what read in from samtools/bamtools
*/


/*
void contrast_clust(
    const vector<bool>& xLfwd_bvec, const vector<string>& xLchr_svec, const vector<ul>& xLst_uvec, const vector<ul>& xLed_uvec, const vector<bool>& xRfwd_bvec, const vector<string>& xRchr_svec, const vector<ul>& xRst_uvec, const vector<ul>& xRed_uvec,
    const vector<bool>& yLfwd_bvec, const vector<string>& yLchr_svec, const vector<ul>& yLst_uvec, const vector<ul>& yLed_uvec, const vector<bool>& yRfwd_bvec, const vector<string>& yRchr_svec, const vector<ul>& yRst_uvec, const vector<ul>& yRed_uvec,
    const vector<string>& chr_svec, const vector<ul>& chr_size_uvec, int x_sup, int y_sup,
    vector<SV>& svs) {



  map<pair<bool,string>,ull> fwdchr_to_offset = make_offset(chr_svec, chr_size_uvec);
  map<ull,pair<bool,string> > offset_to_fwdchr = make_reverse(fwdchr_to_offset);
  vector<rdp> x_rdp_vec = anch_to_coord(xLfwd_bvec,xLchr_svec,xLst_uvec,xLed_uvec,xRfwd_bvec,xRchr_svec,xRst_uvec,xRed_uvec,fwdchr_to_offset);
  vector<rdp> y_rdp_vec = anch_to_coord(yLfwd_bvec,yLchr_svec,yLst_uvec,yLed_uvec,yRfwd_bvec,yRchr_svec,yRst_uvec,yRed_uvec,fwdchr_to_offset);
  vector<evt> x_evt_vec = coord_to_evt(x_rdp_vec);
  stable_sort(x_evt_vec.begin(), x_evt_vec.end(), compare_as_evt);
  vector<evt> y_evt_vec = coord_to_evt(y_rdp_vec);
  stable_sort(y_evt_vec.begin(), y_evt_vec.end(), compare_as_evt);
  vector<clust> x_clust_vec = scan_clust(x_evt_vec,x_rdp_vec,overlap_both);
  vector<clust> y_clust_vec = scan_clust(y_evt_vec,y_rdp_vec,overlap_both);
  vector<rdp> x_clust_rdp_vec(x_clust_vec.size());
  vector<rdp> y_clust_rdp_vec(y_clust_vec.size());
  for (ull i = 0; i<x_clust_vec.size(); i++) {
    x_clust_rdp_vec[i] = collapse_clust(x_clust_vec[i],x_rdp_vec);
    //cout<<clust_rdp_vec[i].first.first.first<<" "<<clust_rdp_vec[i].first.first.second<<" ";
    //cout<<clust_rdp_vec[i].first.second.first<<" "<<clust_rdp_vec[i].first.second.second<<endl;
  }
  for (ull i = 0; i<y_clust_vec.size(); i++) { //switch support number sign: + sample, - contrast event
    y_clust_rdp_vec[i] = collapse_clust(y_clust_vec[i],y_rdp_vec);
    y_clust_rdp_vec[i].second = -y_clust_rdp_vec[i].second;
    //cout<<clust_rdp_vec[i].first.first.first<<" "<<clust_rdp_vec[i].first.first.second<<" ";
    //cout<<clust_rdp_vec[i].first.second.first<<" "<<clust_rdp_vec[i].first.second.second<<endl;
  }

  vector<rdp> m_clust_rdp_vec; m_clust_rdp_vec.reserve(x_clust_rdp_vec.size()+y_clust_rdp_vec.size());
  m_clust_rdp_vec.insert(m_clust_rdp_vec.end(), x_clust_rdp_vec.begin(), x_clust_rdp_vec.end());
  m_clust_rdp_vec.insert(m_clust_rdp_vec.end(), y_clust_rdp_vec.begin(), y_clust_rdp_vec.end());
  vector<evt> m_clust_evt_vec = coord_to_evt(m_clust_rdp_vec);
  stable_sort(m_clust_evt_vec.begin(), m_clust_evt_vec.end(), compare_as_evt);
  vector<clust> m_clust_vec = scan_clust(m_clust_evt_vec, m_clust_rdp_vec, overlap_both);
  vector<rdp> clust_rdp_vec;
  for (ull i = 0; i<m_clust_vec.size(); i++) {
    rdp r = collapse_contrast(m_clust_vec[i],m_clust_rdp_vec,x_sup,y_sup); 
    if (r.second>0) clust_rdp_vec.push_back(r);
  }

  size_t n = clust_rdp_vec.size();
  vector<bool> anch_Lfwd = vector<bool>(n);
  vector<string> anch_Lchr = vector<string>(n);
  vector<ul> anch_Lst = vector<ul>(n);
  vector<ul> anch_Led = vector<ul>(n);
  vector<bool> anch_Rfwd = vector<bool>(n);
  vector<string> anch_Rchr = vector<string>(n);
  vector<ul> anch_Rst = vector<ul>(n);
  vector<ul> anch_Red = vector<ul>(n);
  vector<ul> clust_Sup = vector<ul>(n);
  vector<ul> offset_keys;

  for (map<ull,pair<bool,string> >::iterator it = offset_to_fwdchr.begin(); it != offset_to_fwdchr.end(); ++it) {
    offset_keys.push_back(it->first);
    //cout<<(it->second).second<<" "<<(it->second).first<<" "<<(it->first)<<endl;
  }


  for (size_t i = 0; i<n; i++) {
    ull offset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),clust_rdp_vec[i].first.first.first)); //
    anch_Lfwd[i] = offset_to_fwdchr[offset].first;
    anch_Lchr[i] = offset_to_fwdchr[offset].second;
    anch_Lst[i] = clust_rdp_vec[i].first.first.first - offset;
    anch_Led[i] = clust_rdp_vec[i].first.first.second - offset;
    offset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),clust_rdp_vec[i].first.second.first)); //
    anch_Rfwd[i] = offset_to_fwdchr[offset].first;
    anch_Rchr[i] = offset_to_fwdchr[offset].second;
    anch_Rst[i] = clust_rdp_vec[i].first.second.first - offset;
    anch_Red[i] = clust_rdp_vec[i].first.second.second - offset;
    clust_Sup[i] = clust_rdp_vec[i].second;

    // FIXME: store the output in the vector svs
    SV sv;
    sv.st = 0;
    sv.ed = 0;
    svs.push_back(sv);
  }
} */


/*
void anch_clust(
    const vector<bool>& Lfwd_bvec, const vector<string>& Lchr_svec, const vector<ul>& Lst_uvec, const vector<ul>& Led_uvec,
    const vector<bool>& Rfwd_bvec, const vector<string>& Rchr_svec, const vector<ul>& Rst_uvec, const vector<ul>& Red_uvec,
    const vector<string>& chr_svec, const vector<ul>& chr_size_uvec) {
  
  map<pair<bool,string>,ull> fwdchr_to_offset = make_offset(chr_svec, chr_size_uvec);
  map<ull,pair<bool,string> > offset_to_fwdchr = make_reverse(fwdchr_to_offset);
  vector<rdp> rdp_vec = anch_to_coord(Lfwd_bvec,Lchr_svec,Lst_uvec,Led_uvec,Rfwd_bvec,Rchr_svec,Rst_uvec,Red_uvec,fwdchr_to_offset);
  vector<evt> evt_vec = coord_to_evt(rdp_vec);
  stable_sort(evt_vec.begin(), evt_vec.end(), compare_as_evt);
  vector<clust> clust_vec = scan_clust(evt_vec,rdp_vec,overlap_both);

  ull n = clust_vec.size();
  vector<rdp> clust_rdp_vec = vector<rdp>(n);
  vector<bool> anch_Lfwd = vector<bool>(n);
  vector<string> anch_Lchr = vector<string>(n);
  vector<ul> anch_Lst = vector<ul>(n);
  vector<ul> anch_Led = vector<ul>(n);
  vector<bool> anch_Rfwd = vector<bool>(n);
  vector<string> anch_Rchr = vector<string>(n);
  vector<ul> anch_Rst = vector<ul>(n);
  vector<ul> anch_Red = vector<ul>(n);
  vector<ul> clust_Sup = vector<ul>(n);
  vector<ul> offset_keys;

  for (map<ull,pair<bool,string> >::iterator it = offset_to_fwdchr.begin(); it != offset_to_fwdchr.end(); ++it) {
    offset_keys.push_back(it->first);
    //cout<<(it->second).second<<" "<<(it->second).first<<" "<<(it->first)<<endl;
  }

  for (ull i = 0; i<n; i++) {
    clust_rdp_vec[i] = collapse_clust(clust_vec[i],rdp_vec);
    //cout<<clust_rdp_vec[i].first.first.first<<" "<<clust_rdp_vec[i].first.first.second<<" ";
    //cout<<clust_rdp_vec[i].first.second.first<<" "<<clust_rdp_vec[i].first.second.second<<endl;
  }

  for (ull i = 0; i<n; i++) {
    ull offset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),clust_rdp_vec[i].first.first.first)); //
    anch_Lfwd[i] = offset_to_fwdchr[offset].first;
    anch_Lchr[i] = offset_to_fwdchr[offset].second;
    anch_Lst[i] = clust_rdp_vec[i].first.first.first - offset;
    anch_Led[i] = clust_rdp_vec[i].first.first.second - offset;
    offset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),clust_rdp_vec[i].first.second.first)); //
    anch_Rfwd[i] = offset_to_fwdchr[offset].first;
    anch_Rchr[i] = offset_to_fwdchr[offset].second;
    anch_Rst[i] = clust_rdp_vec[i].first.second.first - offset;
    anch_Red[i] = clust_rdp_vec[i].first.second.second - offset;
    clust_Sup[i] = clust_rdp_vec[i].second;
  }

  //Rcpp::List ret;
  //ret["Lfwd"]=Rcpp::wrap(anch_Lfwd); ret["Lchr"]=Rcpp::wrap(anch_Lchr); ret["Lst"]=Rcpp::wrap(anch_Lst); ret["Led"]=Rcpp::wrap(anch_Led);
  //ret["Rfwd"]=Rcpp::wrap(anch_Rfwd); ret["Rchr"]=Rcpp::wrap(anch_Rchr); ret["Rst"]=Rcpp::wrap(anch_Rst); ret["Red"]=Rcpp::wrap(anch_Red);
  //ret["Sup"]=Rcpp::wrap(clust_Sup);
  
  //return ret;
}
*/
