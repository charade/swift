// g++ anch.cpp -o anch -L../bamtools/lib -lbamtools -I../bamtools/include
// g++ -c -pthread -I./boost/include -I./bamtools/include -I./bedtools/src/utils/bedFilePE -I./bedtools/src/utils/bedFile -I./bedtools/src/utils/lineFileUtilities -I./bedtools/src/utils/gzstream -I./bedtools/src/utils/fileType -W -Wall -Wno-unknown-pragmas -Wfatal-errors -O3 -DNOPENMP clust/anch.cpp -o clust/anch.o -I.

	/* DEBUG RDP
  for(vector<rdp>::iterator vit=allRDP.begin(); vit!=allRDP.end(); ++vit) {
    show_rdp(*vit); cout<<endl;
  }
  exit(1);
	*/

#include "anch.h"

//typedef unsigned long ul;                   //shorthand for unsigned long (32bit), 4G
//typedef unsigned long long ull;             //shorthand for unsinged long long (64bit), 4G**2
// const int BAM_ALIGNMENT_PAIRED              = 0x0001;
// const int BAM_ALIGNMENT_PROPER_PAIR         = 0x0002;
// const int BAM_ALIGNMENT_UNMAPPED            = 0x0004;
// const int BAM_ALIGNMENT_MATE_UNMAPPED       = 0x0008;
// const int BAM_ALIGNMENT_REVERSE_STRAND      = 0x0010;
// const int BAM_ALIGNMENT_MATE_REVERSE_STRAND = 0x0020;
// const int BAM_ALIGNMENT_READ_1              = 0x0040;
// const int BAM_ALIGNMENT_READ_2              = 0x0080;
// const int BAM_ALIGNMENT_SECONDARY           = 0x0100;
// const int BAM_ALIGNMENT_QC_FAILED           = 0x0200;
// const int BAM_ALIGNMENT_DUPLICATE           = 0x0400;


namespace BamTools {

// ---------------------------------------------
// CoverageVisitor implementation

class CoverageVisitor : public PileupVisitor {

						//This Visitor writes coverage to an unordered_map of global index
						//note strand is always true is location is without strandness

    public:
        CoverageVisitor(const RefVector& references, unordered_map<ull,int>& cov) 
            : PileupVisitor()
            , m_references(references)
            , m_cov(cov)
        { 
					vector<string> chr_svec; 
					vector<ul> chr_size_uvec;
  				for (size_t i = 0; i < m_references.size(); ++i) {
    				chr_svec.push_back(m_references[i].RefName);
    				chr_size_uvec.push_back(m_references[i].RefLength);
  				}
					m_fwdchr_to_offset = make_offset(chr_svec, chr_size_uvec);
				}

        ~CoverageVisitor(void) { }

    // PileupVisitor interface implementation
    public:
    // prints coverage results ( tab-delimited )
        void Visit(const PileupPosition& pileupData) {
						//m_key = m_fwdchr_to_offset.at(make_pair(true,m_references[pileupData.RefId].RefName)) + pileupData.Position;
						//generate global index key for (chr,pos,strand==false)
						//m_cov[m_key] = pileupData.PileupAlignments.size();
						//insert coverage to the indexed position
        }

    private:
        RefVector m_references;
				ull m_key;
				map<pair<bool,string>,ull> m_fwdchr_to_offset;
        unordered_map<ull,int>&  m_cov;
};

} // namespace BamTools

// ---------------------------------------------
// CoverageSettings implementation

struct CoverageSettings {

    // flags
    bool HasInputFile;
    bool HasScanRegion;

    // filenames
    string InputBamFilename;
    string scanRegion;

    // constructor
    CoverageSettings(string InputBamFilename, string scanRegion)
        : HasInputFile(false)
        , HasScanRegion(false)
        , InputBamFilename(InputBamFilename)
        , scanRegion(scanRegion)
    { }
};

// ---------------------------------------------
// CoverageToolPrivate implementation

struct CoverageToolPrivate {

    // ctor & dtor
    public:
        CoverageToolPrivate(CoverageSettings& settings, unordered_map<ull,int>& cov)
            : m_settings(settings)
            , m_cov(cov) // init to an empty pair<string,int>
        { }

        ~CoverageToolPrivate(void) { }

    // interface
    public:
        bool Run(void);

    // data members
    private:
        CoverageSettings& m_settings;
				unordered_map<ull,int>& m_cov;
        BamTools::RefVector m_references;
};

bool CoverageToolPrivate::Run(void) {

    //open our BAM reader
    BamTools::BamReader reader;
    if ( !reader.Open(m_settings.InputBamFilename) ) {
        cerr << "bamtools coverage ERROR: could not open input BAM file: " << m_settings.InputBamFilename << endl;
        return false;
    }

		// set scan region
		BamTools::BamRegion region;
		BamTools::Utilities::ParseRegionString(m_settings.scanRegion,reader,region);

    // retrieve references
    m_references = reader.GetReferenceData();

    // set up our output 'visitor'
    BamTools::CoverageVisitor* cv = new BamTools::CoverageVisitor(m_references, m_cov);

    // set up pileup engine with 'visitor'
    BamTools::PileupEngine pileup;
    pileup.AddVisitor(cv);

    // process input data
    BamTools::BamAlignment al;
    while ( reader.GetNextAlignment(al) )
        pileup.AddAlignment(al);
    pileup.Flush();

    // clean up
    reader.Close();
		delete cv; //free dynamically allocated memory
    cv = 0;

    // return success
    return true;
}

// NOTE: code adapted from bamtools/src/toolkit/bamtools_coverage.cpp
//       to use:
//          unordered_map<ull,int> cov;
//       		CoverageSettings settings(bamName, regionString);
//					CoverageToolPrivate runner(settings, cov);
//					runner.Run();

bool region_to_coverage(string bamName, string regionString, unordered_map<ull,int>& cov) {

	cerr<<"region_to_coverage: "<<bamName<<" ...";
	time_t start = time(0); 

	CoverageSettings settings(bamName, regionString);
	CoverageToolPrivate runner(settings, cov);
	runner.Run();

	time_t end = time(0); 
	double time = difftime(end, start);
  cerr<<"done. ("<<time<<"s)"<<endl;
	
	return true;
}

// NOTE: secondary alignment is now ALLOWED (+2048) !
static bool is_concordant(uint32_t flag) { // concordant
  return
  flag == 99 ||  flag == 99+2048 || // 0110 0011
  flag == 147 || flag == 147+2048 || // 1001 0011
  flag == 83 ||  flag == 83+2048 || // 0101 0011
  flag == 163 || flag == 163+2048 ||  // 1010 0011
  flag == 97  || flag == 97+2048  || // 0110 0001
  flag == 145 || flag == 145+2048 ||  // 1001 0001
  flag == 81  || flag == 81+2048 || // 0101 0001
  flag == 161 || flag == 161+2048;   // 1010 0001
} // rdp: all FR pairs, regardless of aligner's (improper flag)

static bool is_hang(uint32_t flag) {
  return
  flag == 73 || flag == 73+2048 ||  // 0100 1001
  flag == 105 || flag == 105+2048 || // 
  flag == 89 || flag == 89+2048 || // 0101 1001
  flag == 121 || flag == 121+2048 ||  // 
  flag == 137 || flag == 137+2048 || // 1000 1001
  flag == 169 || flag == 169+2048 || //
  flag == 153 || flag == 153+2048 || // 1001 1001
  flag == 185 || flag == 185+2048;   //
} // rdp: all F* or R* pairs
  // NOTE: 73=105 1m+, 89=121 1m-, 137=169 2m+, 153 == 185 2m-
  // 1m+ is 1st read mapped to forward, try decode 1m-, 2m+, 2m- similarily
  // 89, 153, 105, 169 never exists though, or is it?

static bool is_discordant(uint32_t flag) { // discordant
  return
  flag == 65  || flag == 65+2048  || // 0100 0001
  flag == 129 || flag == 129+2048 ||// 1000 0001
  flag == 113 || flag == 113+2048 ||// 0111 0001
  flag == 177 || flag == 177+2048 ||// 1011 0001
  flag == 67  || flag == 67+2048  ||// 
  flag == 115 || flag == 115+2048 ||// 
  flag == 131 || flag == 131+2048 ||// 
  flag == 179 || flag == 179+2048;   // 
} //rdp: all same direction FF and RR with correct/incorrect TLEN

static bool is_impp(uint32_t flag) { // marked improper by aligner (used as default)
  return
  flag == 97  || flag == 97+2048 ||// 0110 0001
  flag == 145 || flag == 145+2048 ||// 1001 0001
  flag == 81  || flag == 81+2048  ||// 0101 0001
  flag == 161 || flag == 161+2048;   // 1010 0001
} //impp: FR pair with TLEN<=0 or TLEN>big (as determined by aligner)
  //      or RNEXT is another chr
  //FROM BWA:
  //BWA estimates the insert size distribution per 2048*1024 read pairs. It first collects pairs of reads with both ends mapped with a single-end quality 20 or higher and then calculates median (Q2), lower and higher quartile (Q1 and Q3). It estimates the mean and the variance of the insert size distribution from pairs whose insert sizes are within interval [Q1-2(Q3-Q1), Q3+2(Q3-Q1)]. The maximum distance x for a pair considered to be properly paired (SAM flag 0x2) is calculated by solving equation Phi((x-mu)/sigma)=x/L*p0, where mu is the mean, sigma is the standard error of the insert size distribution, L is the length of the genome, p0 is prior of anomalous pair and Phi() is the standard cumulative distribution function. For mapping Illumina short-insert reads to the human genome, x is about 6-7 sigma away from the mean. Quartiles, mean, variance and x will be printed to the standard error output.

// Math functions

template<typename TIterator, typename TValue>
static void getMedian(TIterator begin, TIterator end, TValue& median) {
  nth_element(begin, begin + (end - begin) / 2, end);
  median = *(begin + (end - begin) / 2);
}

template<typename TIterator, typename TValue>
static void getMAD(TIterator begin, TIterator end, TValue median, TValue& mad) {
  vector<TValue> absDev;
  for (;begin<end;++begin)
    absDev.push_back(       abs( (int)( (TValue)*begin - median  ) )     );
  getMedian(absDev.begin(), absDev.end(), mad);
}

template<typename TIterator, typename TValue>
static void getMean(TIterator begin, TIterator end, TValue& mean) {
  mean = 0;
  unsigned int count = 0;
  for (; begin<end; ++begin,++count) mean += *begin;
  mean /= count;
}

template<typename TIterator, typename TValue>
static void getStdDev(TIterator begin, TIterator end, TValue mean, TValue& stdDev) {
  stdDev = 0;
  unsigned int count = 0;
  for (;begin<end;++begin,++count) stdDev += ((TValue)*begin - mean) * ((TValue)*begin - mean);
  stdDev = sqrt(stdDev / (TValue) count);
}

// End math functions

static unsigned int qwidthFromCigar(const vector<BamTools::CigarOp>& cigar) {
  unsigned int qwidth = 0;
  for (unsigned int i = 0; i < cigar.size(); ++i) {
    char type = cigar[i].Type;
    if (type == 'M' || type == 'I' || type == 'S' || type == '=' || type == 'X') {
      qwidth += cigar[i].Length;
    }
  }
  return qwidth;
}

static bool filter(const vector<string>& bamPath,
									 const msi& fn2fi,
									 vector<rdp>& rdp_vec,
									 msd& bstat, 
									 //IMPROVE:
                   //  this is default to what learned from multibam reader
                   //  how about bams have different stats?
                   //  we might need to use BamReader, iterate through and sort the filtered rdps 
									 const map<pair<bool,string>,ull>& fwdchr_to_offset, 
									 const BamTools::RefVector& references, 
									 const string& scanRegion,
									 bool trustFlag=false //, bool useSplit=false 
									 
  ) {

	time_t start = time(0); 

  int32_t medianRL = 0;
  int32_t bigDel = 0;
	int32_t smallIns = 0;

	if(bamPath.size()==0) return true;
	cerr<<"filter: filtering "<<bamPath[0]<<" etc, ...";

	//Set Bamfile and regions
  BamTools::BamMultiReader reader;
	BamTools::BamRegion region;
  reader.Open(bamPath);
	vector<string> bamIndex(bamPath);
	for(vector<string>::iterator vit=bamIndex.begin(); vit!=bamIndex.end(); ++vit) (*vit).append(".bai");
  if (!reader.OpenIndexes(bamIndex)) {
    cerr << "Missing bam index file: ";
		ostream_iterator<string> out_it(cerr,", ");
		copy( bamPath.begin(), bamPath.end(), out_it ); 
		cerr << endl;
    reader.Close();
    return false;
  }
	if (!BamTools::Utilities::ParseRegionString(scanRegion,reader,region)) {
		cerr << "Clust Without SetRegion: LeftRefID[" << region.LeftRefID << "],LeftPosition[" << region.LeftPosition << "],RightRefID[" << region.RightRefID << "],RightPosition[" << region.RightPosition << "];" << endl;
	}
		
	/* DEBUG
	cerr << "LeftRefID" << region.LeftRefID << "LeftPosition" << region.LeftPosition << "RightRefID" << region.RightRefID << "RightPosition" << region.RightPosition << endl;
	cerr << "scanRegion" << scanRegion << endl;
	BamTools::Utilities::ParseRegionString(scanRegion,reader,region);
	cerr << "LeftRefID" << region.LeftRefID << "LeftPosition" << region.LeftPosition << "RightRefID" << region.RightRefID << "RightPosition" << region.RightPosition << endl;
	*/

  ba al;         // Contain current alignments 
  mvba sa;       // Contain current rdp origin

  // Calculate statistical parameters
  vector<unsigned int> vecISize;
  vector<unsigned int> vecRL;
  unsigned int maxNumPairs=10000;  // Maximum number of pairs used to estimate library parameters

	if (!reader.SetRegion(region)) {
		cerr << "Fail to set bam region: "; return false;
	}
  while (reader.GetNextAlignmentCore(al) ) {
    if ((al.AlignmentFlag & 0x0001) && !(al.AlignmentFlag & 0x0004) && !(al.AlignmentFlag & 0x0008) && (al.AlignmentFlag & 0x0040) && (al.RefID==al.MateRefID) && !(al.AlignmentFlag & 0x0100) && !(al.AlignmentFlag & 0x0200)  && !(al.AlignmentFlag & 0x0400) && !(al.AlignmentFlag & 0x0800)) {
      vecISize.push_back(abs(al.InsertSize));
      vecRL.push_back(qwidthFromCigar(al.CigarData));

      if (vecISize.size() >= maxNumPairs) {
        break;
      }
    }
  }

  getMedian(vecRL.begin(), vecRL.end(), medianRL);
  unsigned int medianISize, madISize;
  getMedian(vecISize.begin(), vecISize.end(), medianISize);
  getMAD(vecISize.begin(), vecISize.end(), medianISize, madISize);
  bigDel = medianISize + 3 * madISize;       // NOTE: this affects the limit of detection
	smallIns = medianISize - 2 * madISize;     // NOTE: this affects the limit of detection

	cerr<<"medianRL="<<medianRL<<","; bstat["medianRL"]=medianRL;
	cerr<<"medianISize="<<medianISize<<","; bstat["medianISize"]=medianISize;
	cerr<<"bigDel="<<bigDel<<","; bstat["bigDel"]=bigDel;
	cerr<<"smallIns="<<smallIns<<","; bstat["smallIns"]=smallIns;

	//IMPROVE: we may use Nancy's robust method here
	//         smallIns = medianISize - 2 * sdL
	//         bigDel = medianISize + 3 * sdR

	//IMPROVE: function should be able to do multiple filtering: is, sc, sp
	// 	if mode = "is", filter reads by Insert Size

	/* Rewind is not needed if SetRegion is used
	if( !reader.Rewind() ) {
		cerr<<"can't rewind"<<endl; return false;
	}
	*/

	if (!reader.SetRegion(region)) {
    cerr << "Fail to set bam region: "; return false;
  }

  //reader.Jump(0, 0); // jump to the beginning of the bam file
  int acnt(0);
  while (reader.GetNextAlignment(al) ) {
    // check if the rdp has the full information needed, otherwise discard it.
		//cout<<al.RefID<<"-"<<al.Position<<":"<<al.MateRefID<<"-"<<al.MatePosition<<":"<<al.Filename<<":"<<al.Name<<endl; exit(1);
	  if ((acnt++)%1000000==0) cerr<<acnt<<" alignments parsed"<<endl;
    if (!al.IsMapped() || !al.IsMateMapped())
      continue; //continue if it is hanging

		// now keep all FF/RR discordant, 
		//              FR pairs with extreme TLEN
    //              FR pairs with different CHR
    // NOTE:        Left is always defined if:
    //              self.chr<mate.chr || self.chr==mate.chr and self.pos<=mate.pos
    if (
				( 
				  ( al.RefID < al.MateRefID ) || ( al.RefID == al.MateRefID && al.MatePosition >= al.Position )) // self is Lread
      && (
          is_discordant(al.AlignmentFlag) || (trustFlag && is_impp(al.AlignmentFlag)) ||
         ( !trustFlag && // disable|enable self learned statistics
          is_concordant(al.AlignmentFlag) && ( (abs(al.InsertSize) < smallIns || abs(al.InsertSize) > bigDel || al.RefID != al.MateRefID) )
        ))
      ) {
      // LEARN: for const map reference [] operator is offending, need to use non-modifying .at()
      //        for const vector reference [] operator is OK.
      // keep self in L-anch
			ull Loffset = fwdchr_to_offset.at(make_pair(!al.IsReverseStrand(), references[al.RefID].RefName));
      ull Lstart = Loffset + al.Position;
      ull Lend = Loffset + al.Position + qwidthFromCigar(al.CigarData);
			ull Roffset = fwdchr_to_offset.at(make_pair(!al.IsMateReverseStrand(), references[al.MateRefID].RefName));
      ull Rstart = Roffset + al.MatePosition;
      ull Rend = Roffset + al.MatePosition + medianRL;
			sa[fn2fi.at(al.Filename)]=vba(1,ba(al));
			rdp_vec.push_back( 
				(Rstart>=Lstart) ? make_pair(make_pair(make_pair(Lstart,Lend),make_pair(Rstart,Rend)),sa) : make_pair(make_pair(make_pair(Rstart,Rend),make_pair(Lstart,Lend)),sa) 
			); 
			sa.clear();
    }
		
    if (
				( 
				  ( al.MateRefID < al.RefID ) || ( al.RefID == al.MateRefID && al.MatePosition < al.Position )) // mate is Lread
      && (
          is_discordant(al.AlignmentFlag) || 
					(trustFlag && is_impp(al.AlignmentFlag)) ||
         ( !trustFlag &&  // disable|enable self learned statistics
          is_concordant(al.AlignmentFlag) && ( ( abs(al.InsertSize) < smallIns || abs(al.InsertSize) > bigDel || al.RefID != al.MateRefID) )
        ))
      ) {
		  // keep mate in L-anch
			ull Loffset = fwdchr_to_offset.at(make_pair(!al.IsMateReverseStrand(), references[al.MateRefID].RefName));
      ull Lstart = Loffset + al.MatePosition ;
      ull Lend = Loffset + al.MatePosition + medianRL;
			ull Roffset = fwdchr_to_offset.at(make_pair(!al.IsReverseStrand(), references[al.RefID].RefName));
      ull Rstart = Roffset + al.Position;
      ull Rend = Roffset + al.Position + qwidthFromCigar(al.CigarData);
			sa[fn2fi.at(al.Filename)]=vba(1,ba(al));
			rdp_vec.push_back( 
				(Rstart>=Lstart) ? make_pair(make_pair(make_pair(Lstart,Lend),make_pair(Rstart,Rend)),sa) : make_pair(make_pair(make_pair(Rstart,Rend),make_pair(Lstart,Lend)),sa) 
			); 
			sa.clear();
		}
  }

	/* DEBUG
  for(vector<rdp>::iterator vit=rdp_vec.begin(); vit!=rdp_vec.end(); ++vit) {
    show_rdp(*vit); cout<<endl;
  }
	*/

	time_t end = time(0); 
	double time = difftime(end, start);
  cerr<<"done. ("<<time<<"s)"<<endl;

  reader.Close();
  return true;
}

//IMPROVE: in the future, we need to do a class SWIFT 
/*        
	SWIFT {
					public:
           	rdp_vec allRDP;
           	str_vec chr;
           	ul_vec chrSize;
						boost_mapped_vector chrCvg;
						BamTools::scanRegion scanRegion;
						BamTools::MultiBamReader caseBam;
						BamTools::MultiBamReader baseBam;
        	private:
            swift.configure(caseBam, baseBam, scanConfigure);
        		swift.scanRDP();
						swift.filterRDP();
						swift.clustRDP();
            swift.testRDP();
            swift.RDP2BEDPE();
	}
*/

bool do_anchor_clust( const vector<string>& files, const msi& fn2fi, msd& bstat, vector<rdp>& anchors, const string& scanRegion, vector<string>& chr_svec, vector<ul>& chr_size_uvec, map<pair<bool,string>,ull>& fwdchr_to_offset, map<ull,pair<bool,string> >& offset_to_fwdchr, vector<ul>& offset_keys) {

  anchors.clear(); // NOTE: load all alignment read pairs from both case and base samples

  BamTools::BamMultiReader reader;
  reader.Open(files);
  BamTools::RefVector references = reader.GetReferenceData();
	chr_svec.clear(); chr_size_uvec.clear(); 
	fwdchr_to_offset.clear(); offset_to_fwdchr.clear(); offset_keys.clear();
  for (size_t i = 0; i < references.size(); ++i) {
    chr_svec.push_back(references[i].RefName);
    chr_size_uvec.push_back(references[i].RefLength);
  }
  reader.Close(); // ASSUME: require header to be exchangable among bam files

	fwdchr_to_offset = make_offset(chr_svec, chr_size_uvec);
	offset_to_fwdchr = make_reverse(fwdchr_to_offset);
	for (map<ull,pair<bool,string> >::iterator it = offset_to_fwdchr.begin(); it != offset_to_fwdchr.end(); ++it) {
    offset_keys.push_back(it->first);
		/* DEBUG
    cout<<(it->second).second<<" "<<(it->second).first<<" "<<(it->first)<<endl;
		*/
  }

  if (!filter(files, fn2fi, anchors, bstat, fwdchr_to_offset, references, scanRegion))
    return false;

 	//if (!filter(baseBam, allRDP, fwdchr_to_offset, references, scanRegion))
  // 	return false;

	/* DEBUG
  for(vector<rdp>::iterator vit=allRDP.begin(); vit!=allRDP.end(); ++vit) {
    show_rdp(*vit); cout<<endl;
  }
  exit(1);
	*/

	if (!anch_clust(anchors,(ull) bstat["flank"])) // run anch_clust here
		return false;

	/* DEBUG
  for(vector<rdp>::iterator vit=allRDP.begin(); vit!=allRDP.end(); ++vit) {
    show_rdp(*vit); cout<<endl;
  }
  exit(1);
	*/

	/*
	anchors.clear();
	for( vector<rdp>::iterator vit=clust_rdp_vec.begin(); vit!=clust_rdp_vec.end(); ++vit ) {
    rdp_vec.push_back(*vit);
  }
	*/

  return true;
} //this will also fill in for chr_svec, chr_size_uvec and fwdchr_to_offset

bool anchor_to_bedpe( const vector<rdp>& anchors, const vector<string>& files, const map<string,vector<int> >& coverage, const vector<string>& scores, const map<string,vector<int> >& counts, const boost::filesystem::path& bedpefile, const map<ull,pair<bool,string> >& offset_to_fwdchr, const vector<ul>& offset_keys ) {
  //writes coverage, score and count to bedpe format
  //files: f1, f2, ....
  //..., score, ..., cnt1, cov1, cnt2, .cov2, .., note 
	cerr<<"anch_to_bedpe: ...";
	//output bedpe header string in json format
	ofstream bedpe;
	bedpe.open(bedpefile.c_str());
	bedpe<<"#{";
	for( vector<string>::const_iterator sit=files.begin(); sit != files.end(); ++sit ) {
		if( sit != files.begin() ) bedpe<<","; else bedpe<<"\"files\":[";
		bedpe<<"\""<<(*sit)<<"\"";
		if( files.end() - sit < 2 ) bedpe<<"]";
	}
	bedpe<<"}"<<endl;
		
	//output bedpe content with notes
	//IMPROVE: 
  //  we may be able to disable notes later
	for( vector<rdp>::const_iterator it=anchors.begin(); it != anchors.end(); ++it ){
	  // json formatted notes and integer counts: 
	  // note = {fn1:[al11,al12,al13],fn2:[al21,al22,al23],...}
	  // IMPROVE: use a mature json C++ library
	  // NOTE: json only accepts double quote for strings
		string nt("{");
		for( mvba::const_iterator mit = (*it).second.begin();
				mit != (*it).second.end(); ++mit) {
		  string fnt("");
			if( mit != (*it).second.begin() ) fnt.append(","); //more than one filename
			fnt.append("\""+files[(*mit).first]+"\":[");
			for( vector<ba>::const_iterator vit = (*mit).second.begin();
					vit != (*mit).second.end(); ++vit) {
				fnt.append("\""+(*vit).Name+"\"");
				if( (*mit).second.end()-vit>1 ) fnt.append(","); //if not last entry append separator
			}
			fnt.append("]"); nt.append(fnt);
		}
		nt.append("}");
    ull Loffset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),(*it).first.first.first));
    ull Roffset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),(*it).first.second.first));
		bedpe<<offset_to_fwdchr.at(Loffset).second<<"\t";               //CHR1
		bedpe<<(*it).first.first.first - Loffset<<"\t";						  //ST1
		bedpe<<(*it).first.first.second - Loffset<<"\t";						//ED1
		bedpe<<offset_to_fwdchr.at(Roffset).second<<"\t";               //CHR2
		bedpe<<(*it).first.second.first - Loffset<<"\t";						  //ST2
		bedpe<<(*it).first.second.second - Loffset<<"\t";						//ED2
		bedpe<<"CLUST-"<<it-anchors.begin()<<"\t";                  //NAME
		bedpe<<scores.at(it-anchors.begin())<<"\t";                    //SCORE
		bedpe<<offset_to_fwdchr.at(Loffset).first<<"\t";               //FWD1
		bedpe<<offset_to_fwdchr.at(Roffset).first<<"\t";               //FWD2
		for(vector<string>::const_iterator sit=files.begin(); sit != files.end(); ++sit){
			bedpe<<(counts.at(*sit)).at(it-anchors.begin())<<"\t";				//CNT_i
			bedpe<<(coverage.at(*sit)).at(it-anchors.begin())<<"\t";     //COV_i
		}
		bedpe<<nt<<endl;                                            //NOTE
    //FIXME SCORE (json format), e.g.:
		//    [ {"method":"fisher", "stat"="1"; "pvalue"="0.01"}, ... ] 
	}
	cerr<<"done."<<endl;
	return true;
}

//split to anchor_to_count and anchor_to_coverage
bool anchor_to_counts( const vector<rdp>& anchors, const vector<string>& files, const msd& bstat, map<string,vector<int> >& counts) { 
	cerr<<"anchor_to_counts: ...";
	time_t start = time(0);
	for( vector<rdp>::const_iterator it=anchors.begin(); it != anchors.end(); ++it ){
		//assign counts
		for( mvba::const_iterator mit = (*it).second.begin();
				mit != (*it).second.end(); ++mit) {
		  int fcnt(0);
			for( vba::const_iterator vit = (*mit).second.begin();
					vit != (*mit).second.end(); ++vit, ++fcnt) 
			(counts[files[(*mit).first]]).at(it-anchors.begin()) = fcnt;    //CNTi
		}
	}
  time_t end = time(0);
  double time = difftime(end, start);
  cerr<<"done. ("<<time<<"s)"<<endl;
	return true;
}

bool anchor_to_coverage( const vector<rdp>& anchors, const vector<string>& files, const msd& bstat, map<string,vector<int> >& coverage, const map<ull,pair<bool,string> >& offset_to_fwdchr, const vector<ul>& offset_keys ) { 
	cerr<<"anchor_to_coverage: ...";
	time_t start = time(0);
	for( vector<rdp>::const_iterator it=anchors.begin(); it != anchors.end(); ++it ){
		//initialize variables
		if( (ull)(it-anchors.begin()) % 1000000 == 0) cerr << (ull)(it-anchors.begin())+1 << "regions parsed" <<endl; 
    ull Loffset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),(*it).first.first.first));
    ull Roffset = *(--lower_bound(offset_keys.begin(),offset_keys.end(),(*it).first.second.first));
		const string& chr1(offset_to_fwdchr.at(Loffset).second);      //CHR1
		ull st1((*it).first.first.first - Loffset);           //ST1
		ull ed1((*it).first.first.second - Loffset);						//ED1
		const string& chr2(offset_to_fwdchr.at(Roffset).second);               //CHR2
		ull st2((*it).first.second.first - Loffset);						  //ST2
		ull ed2((*it).first.second.second - Loffset);						//ED2
		//assign coverage
		ba al;         // Contain current alignments
  	vba va;        // Contain current rdp origin
		for( vector<string>::const_iterator vit = files.begin(); vit != files.end(); ++vit ){
  		BamTools::BamReader reader;
  		BamTools::BamRegion region;
			int fcov(0);
			string lReg(chr1+":"+to_string(st1)+".."+to_string(ed1));
  		reader.Open(*vit);
			BamTools::Utilities::ParseRegionString(lReg,reader,region);
			reader.SetRegion(region);
			while ( reader.GetNextAlignmentCore(al) ) va.push_back(al);
			reader.Close();
			string rReg(chr2+":"+to_string(st2)+".."+to_string(ed2));
  		reader.Open(*vit);
			BamTools::Utilities::ParseRegionString(rReg,reader,region);
			reader.SetRegion(region);
			while ( reader.GetNextAlignmentCore(al) ) va.push_back(al);
			fcov = va.size()*bstat.at("medianRL")/(ed2-st2+1+ed1-st1+1);
			(coverage[(*vit)]).at(it-anchors.begin()) = fcov;  //COVi
			reader.Close();
		  va.clear();
		}
	}
	time_t end = time(0);
  double time = difftime(end, start);
  cerr<<"done. ("<<time<<"s)"<<endl;
	return true;
}

