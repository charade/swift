#ifndef ANCH_H
#define ANCH_H

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <ctime>
#include <boost/filesystem.hpp>
#include <sstream>
#include "api/BamReader.h"
#include "api/BamIndex.h"
#include "api/BamMultiReader.h"
#include "utils/utils_global.h"
#include "utils/bamtools_utilities.h"
#include "utils/bamtools_options.h"
#include "utils/bamtools_pileup_engine.h"
#include "libswan_clust.h"
//#include <boost/unordered_map.hpp>
//#include "bedFilePE.h"
//#include "../sv.h"

using namespace std;

bool region_to_coverage(string bamName, string regionString, unordered_map<ull,int>& cov);

bool do_anchor_clust( const vector<string>& files, const msi& fn2fi, msd& bstat, vector<rdp>& anchors, const string& scanRegion, vector<string>& chr_svec, vector<ul>& chr_size_uvec, map<pair<bool,string>,ull>& fwdchr_to_offset, map<ull,pair<bool,string> >& offset_to_fwdchr, vector<ul>& offset_keys);

bool anchor_to_bedpe( const vector<rdp>& anchors, const vector<string>& files, const map<string,vector<int> >& coverage, const vector<string>& scores, const map<string,vector<int> >& counts, const boost::filesystem::path& bedpefile, const map<ull,pair<bool,string> >& offset_to_fwdchr, const vector<ul>& offset_keys );

bool anchor_to_counts( const vector<rdp>& anchors, const vector<string>& files, const msd& bstat, map<string,vector<int> >& counts);

bool anchor_to_coverage( const vector<rdp>& anchors, const vector<string>& files, const msd& bstat, map<string,vector<int> >& coverage, const map<ull,pair<bool,string> >& offset_to_fwdchr, const vector<ul>& offset_keys );

#endif
