#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem.hpp>
#include "libswan_sclip.h"

#define max_chr_len 3e8

using namespace std;

// Config arguments
struct Config {
  std::string chromosomeName;
  int minReadPerCluster;
  int minBasePerCluster;
  int scanStart;
  int scanEnd;
  int trunkSize;
  std::string spout;
	std::vector<boost::filesystem::path> files;
};

int main(int argc, char **argv) {
  Config c;

  // Define generic options
  boost::program_options::options_description generic("Generic options");
  generic.add_options()
    ("help,?", "show help message")
    ("chromosomeName,c", boost::program_options::value<std::string>(&c.chromosomeName)->default_value("11"), "chromosome to scan")
    ("minReadPerCluster,i", boost::program_options::value<int>(&c.minReadPerCluster)->default_value(3), "minimal number of reads per cluster")
    ("minBasePerCluster,j", boost::program_options::value<int>(&c.minBasePerCluster)->default_value(30), "minimal number of total bases per cluster")
    ("scanStart,u", boost::program_options::value<int>(&c.scanStart)->default_value(1), "1-indexed scan start")
    ("scanEnd,v", boost::program_options::value<int>(&c.scanEnd)->default_value(max_chr_len), "1-indexed scan end")
    ("trunkSize,z", boost::program_options::value<int>(&c.trunkSize)->default_value(1000000), "trunk size for scanning bamfile")
    ("spout,o", boost::program_options::value<std::string>(&c.spout)->default_value("input"), "sample output prefix")
    ;

  // Define hidden options
  boost::program_options::options_description hidden("Hidden options");
  hidden.add_options()
    ("input-file", boost::program_options::value< std::vector<boost::filesystem::path> >(&c.files), "input file")
    ;

  boost::program_options::positional_options_description pos_args;
  pos_args.add("input-file", -1);
  
  // Set the visibility
  boost::program_options::options_description cmdline_options;
  cmdline_options.add(generic).add(hidden);
  boost::program_options::options_description visible_options;
  visible_options.add(generic);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).positional(pos_args).run(), vm);
  boost::program_options::notify(vm);
  
  // Check command line arguments
  if ((vm.count("help")) || (!vm.count("input-file"))) {
    // TODO: fix
    std::cout << "Usage: " << argv[0] << " [OPTIONS] <sample1.bam> <sample2.bam> ..." << std::endl;
    std::cout << visible_options << "\n";
    return 1;
  }
  
  // Show cmd
  for(int i=0; i<argc; ++i) { std::cout << argv[i] << ' '; }
  std::cout << std::endl;
  
  // Main program
  int n_trunks = ceil((double)(c.scanEnd - c.scanStart + 1) / c.trunkSize);
  double SC_PROPORTION_MISMATCH_THRESH = 0.05;
  int MIN_GAP_DISTANCE = 10000;
  vector<string> files;
  for (size_t i = 0; i < c.files.size(); ++i) {
    files.push_back(c.files[i].string());
  }
  
  for (int ti = 1; ti <= n_trunks; ++ti) {
    cpp_core_sclip(ti, n_trunks, c.scanStart, c.scanEnd, c.trunkSize, files, c.chromosomeName,
      c.minReadPerCluster, c.minBasePerCluster, SC_PROPORTION_MISMATCH_THRESH, MIN_GAP_DISTANCE);
  }
  
  return 0;
}
