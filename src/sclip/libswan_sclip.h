#ifndef _LIBSWAN_SCLIP
#define _LIBSWAN_SCLIP

#include <vector>
#include <string>

void cpp_core_sclip(int ti, int n_trunks, int scan_start, int scan_end,
  int trunk_size, const std::vector<std::string>& files, const std::string& seqname,
  int MIN_READS_PER_CLUSTER, int MIN_BASES_PER_CLUSTER, double SC_PROPORTION_MISMATCH_THRESH,
  int MIN_GAP_DISTANCE);


#endif
