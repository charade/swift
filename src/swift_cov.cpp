#include <cassert>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <boost/iostreams/filtering_stream.hpp>    
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/operations.hpp>
#include <boost/iostreams/write.hpp>
#include <boost/iostreams/read.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include "clust/anch.h"
#include <string>
#include <sparsehash/sparse_hash_map>
#include <gzlog.h>
#include <zlib.h>
/*
#include "api/BamReader.h"
#include "api/BamMultiReader.h"
#include "utils/utils_global.h"
#include "utils/bamtools_utilities.h"
*/

using namespace std;
namespace bo = boost::iostreams;

typedef google::sparse_hash_map<int, int> umap;

struct MasterConfig {
  string region;
  string bamfile;
  string outfile;
	bool verify;
};

struct IntToIntSerializer {
  bool operator()(FILE* fp, const std::pair<const int, int>& value) const {
    // Write the key.  We ignore endianness for this example.
    if (fwrite(&value.first, sizeof(value.first), 1, fp) != 1)
      return false;
    // Write the value.
    //assert(value.second.length() <= 255);   // we only support writing small strings
    //const unsigned char size = value.second.length();
    const unsigned char size = sizeof(value.second);
    if (fwrite(&size, 1, 1, fp) != 1)
      return false;
    if (fwrite(&value.second, sizeof(value.second), 1, fp) != 1)
      return false;
    return true;
  }
  bool operator()(FILE* fp, std::pair<const int, int>* value) const {
    // Read the key.  Note the need for const_cast to get around
    // the fact hash_map keys are always const.
    if (fread(const_cast<int*>(&value->first), sizeof(value->first), 1, fp) != 1)
      return false;
    // Read the value.
    unsigned char size;    // all strings are <= 255 chars long
    if (fread(&size, 1, 1, fp) != 1)
      return false;
    if (fread(&value->second, sizeof(value->second), 1, fp) != 1)
      return false;
    return true;
  }
  bool operator()(  bo::filtering_streambuf<bo::output>& ofs, const std::pair<const int, int>& value) const {
    // Write the key.  We ignore endianness for this example.
    if (ofs.write(&value.first, 1) != 1)
      return false;
    // Write the value.
    //assert(value.second.length() <= 255);   // we only support writing small strings
    //const unsigned char size = value.second.length();
    // const unsigned char size = sizeof(value.second);
    if (ofs.write(&value.second, 1) != 1)
      return false;
    //if (gzlog_write(gz, &size, 1) != 1)
    //  return false;
    //if (ofs.write(&value.second, sizeof(value.second) ) != sizeof(value.second))
    //  return false;
    //if (gzlog_write(gz, &value.second, sizeof(value.second)) != 1)
    //  return false;
    return true;
  }
  bool operator()(  bo::filtering_streambuf<bo::input>& ifs, std::pair<const int, int>* value) const {
    // Read the key.  Note the need for const_cast to get around
    // the fact hash_map keys are always const.
    if (ifs.read(const_cast<int*>(&value->first), 1) != 1)
      return false;
    // Read the value.
    if (ifs.read(const_cast<int*>(&value->second), 1) != 1)
      return false;
    return true;
  }
};

int main(int argc, char **argv) {

	MasterConfig c;
	umap cov;
	BamTools::BamAlignment al;
	
  boost::program_options::options_description generic("Generic options");
  generic.add_options()
    ("help,?", "show help message")
    ("bamfile,b", boost::program_options::value<string>(&c.bamfile)->default_value(""), "BAM file")
    ("outfile,o", boost::program_options::value<string>(&c.outfile)->default_value("swift.cov"), "Coverage output file in google sparsehash format")
    ("verify,v", boost::program_options::value<bool>(&c.verify)->default_value(false), "verify outfile")
    ("region,r", boost::program_options::value<string>(&c.region)->default_value(""), "region to scan, e.g.: chr1 | chr1:500 | chr1:500..1000, only ONE chr allowed")
    ;
  boost::program_options::positional_options_description pos_args;

  // Set the visibility
  boost::program_options::options_description cmdline_options;
  cmdline_options.add(generic);
  boost::program_options::options_description visible_options;
  visible_options.add(generic);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).positional(pos_args).run(), vm);

  // Check command line arguments
  if (vm.count("help")) {
    cout << "Usage: " << argv[0] << " [OPTIONS] -b <bam> -o <cov> " << endl;
    cout << visible_options << "\n";
    return 1;
  }
 
  boost::program_options::notify(vm); //raises option error
	string scanRegion(c.region);

	BamTools::BamReader reader;
  if ( !reader.Open(c.bamfile) ) {
      cerr << "bamtools coverage ERROR: could not open input BAM file: " << c.bamfile << endl;
      return false;
  }

	BamTools::BamRegion region;
	cerr << "scanning: " << scanRegion << endl;
  BamTools::Utilities::ParseRegionString(scanRegion,reader,region);
	assert (region.LeftRefID==region.RightRefID);   //enforce one chr
	reader.SetRegion(region);

	/*
  if ( !reader.SetRegion(region) ) {
    cerr << "Fail to set bam region: "; 
		cerr << "Tried SetRegion: LeftRefID[" << region.LeftRefID << "],LeftPosition[" << region.LeftPosition << "],RightRefID[" << region.RightRefID << "],RightPosition[" << region.RightPosition << "];" << endl;
		return false;
  }*/

	while (reader.GetNextAlignmentCore(al) ) {
		if (!al.IsMapped()) 
			continue;
		umap::iterator idx( cov.find(al.Position) );
		if( idx != cov.end() ) 
			(*idx).second++;
		else
			cov[al.Position] = 1;
  }

	reader.Close();
	cerr <<"map size "<<cov.size()<<endl;

	FILE* outfile = fopen(c.outfile.c_str(), "w");	
  cov.serialize(IntToIntSerializer(), outfile);
  fclose(outfile);
	
	if(c.verify) {
  	umap vf;
    FILE* infile = fopen(c.outfile.c_str(), "r");
    vf.unserialize(IntToIntSerializer(), infile);
		umap::const_iterator uit = vf.begin();
		for(;uit!=vf.end();++uit)
			assert ((*uit).second == cov[(*uit).first]);
		fclose(infile);
	}

	/*
	FILE* outfile = fopen(c.outfile.c_str(), "w");	
  cov.serialize(IntToIntSerializer(), outfile);
  fclose(outfile);
	
	if(c.verify) {
  	umap vf;
    FILE* infile = fopen(c.outfile.c_str(), "r");
    vf.unserialize(IntToIntSerializer(), infile);
		umap::const_iterator uit = vf.begin();
		for(;uit!=vf.end();++uit)
			assert ((*uit).second == cov[(*uit).first]);
		fclose(infile);
	}
	*/

	return 1;
}
