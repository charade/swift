#ifndef VCF_H
#define VCF_H

#include <vector>
#include <list>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <assert.h>
#include <set>
#include <algorithm>

using namespace std;

namespace vcf {

class Variant;

enum VariantFieldType { FIELD_FLOAT = 0
                      , FIELD_INTEGER
                      , FIELD_BOOL
                      , FIELD_STRING
                      , FIELD_UNKNOWN
                      };

enum VariantFieldNumber { ALLELE_NUMBER = -2
                        , GENOTYPE_NUMBER = -1
                        };

const int INDEX_NONE = -1;


VariantFieldType typeStrToFieldType(string& typeStr);
ostream& operator<<(ostream& out, VariantFieldType type);

typedef map<string, map<string, vector<string> > > Samples;
typedef vector<pair<int, string> > Cigar;

class VariantCallFile {

public:

  istream* file;

  bool usingTabix;

  string header;
  string line; // the current line
  string fileformat;
  string fileDate;
  string source;
  string reference;
  string phasing;
  map<string, VariantFieldType> infoTypes;
  map<string, int> infoCounts;
  map<string, VariantFieldType> formatTypes;
  map<string, int> formatCounts;
  vector<string> sampleNames;
  bool parseSamples;
  bool _done;

  void updateSamples(vector<string>& newSampleNames);
  string headerWithSampleNames(vector<string>& newSamples); // non-destructive, for output
  void addHeaderLine(string line);
  void addHeaderInfoLine(string line);
  void removeInfoHeaderLine(string line);
  void removeGenoHeaderLine(string line);
  vector<string> infoIds(void);
  vector<string> formatIds(void);

  bool open(const string& filename) {
    return openFile(filename);
  }

  bool openFile(const string& filename) {
      file = &_file;
      _file.open(filename.c_str(), ifstream::in);
      parsedHeader = parseHeader();
      return parsedHeader;
  }

  bool open(istream& stream) {
      file = &stream;
      parsedHeader = parseHeader();
      return parsedHeader;
  }

  bool open(ifstream& stream) {
      file = &stream;
      parsedHeader = parseHeader();
      return parsedHeader;
  }

  bool openForOutput(string& headerStr) {
      parsedHeader = parseHeader(headerStr);
      return parsedHeader;
  }

  VariantCallFile(void) : usingTabix(false), parseSamples(true), justSetRegion(false), parsedHeader(false) { }
  ~VariantCallFile(void) {
  }

  bool is_open(void) { return parsedHeader; }

  bool eof(void) { return _file.eof(); }

  bool done(void) { return _done; }

  bool parseHeader(string& headerStr);

  bool parseHeader(void);

  bool getNextVariant(Variant& var);

private:
  bool firstRecord;
  bool justSetRegion;
  bool usingFile;
  ifstream _file;
  bool parsedHeader;
};






class Variant {

  friend ostream& operator<<(ostream& out, Variant& var);
  
public:

  string chrom;
  long position;
  long zeroBasedPosition(void);
  string id;
  string ref;
  string alt;

  map<string, string> extendedAlternates(long int newPosition, long int length);

  string originalLine; // the literal of the record, as read
  // TODO
  // the ordering of genotypes for the likelihoods is given by: F(j/k) = (k*(k+1)/2)+j
  // vector<pair<int, int> > genotypes;  // indexes into the alleles, ordered as per the spec
  string filter;
  string quality;
  VariantFieldType infoType(string& key);
  map<string, string> info;
  map<string, bool> infoFlags;
  VariantFieldType formatType(string& key);
  vector<string> format;
  map<string, map<string, string> > samples;  // vector<string> allows for lists by Genotypes or Alternates
  vector<string> sampleNames;
  vector<string> outputSampleNames;
  VariantCallFile* vcf;

  //void addInfoInt(string& tag, int value);
  //void addInfoFloat(string& tag, double value);
  //void addInfoString(string& tag, string& value);

public:

  Variant() { }

  Variant(VariantCallFile& v)
      : sampleNames(v.sampleNames)
      , outputSampleNames(v.sampleNames)
      , vcf(&v)
  { }

  void setVariantCallFile(VariantCallFile& v);
  void setVariantCallFile(VariantCallFile* v);

  void parse(string& line, bool parseSamples = true);
  void addFilter(string& tag);
  bool getValueBool(string& key, string& sample, int index = INDEX_NONE);
  double getValueFloat(string& key, string& sample, int index = INDEX_NONE);
  string getValueString(string& key, string& sample, int index = INDEX_NONE);
  bool getSampleValueBool(string& key, string& sample, int index = INDEX_NONE);
  double getSampleValueFloat(string& key, string& sample, int index = INDEX_NONE);
  string getSampleValueString(string& key, string& sample, int index = INDEX_NONE);
  bool getInfoValueBool(string& key, int index = INDEX_NONE);
  double getInfoValueFloat(string& key, int index = INDEX_NONE);
  string getInfoValueString(string& key, int index = INDEX_NONE);
  void addFormatField(string& key);
  void setOutputSampleNames(vector<string>& outputSamples);
  int getNumSamples(void);
  //bool isPhased(void);
  // TODO
  //void setInfoField(string& key, string& val);

private:

  string lastFormat;

};



string unionInfoHeaderLines(string& s1, string& s2);



vector<string>& unique(vector<string>& strings);


} // end namespace VCF

#endif
