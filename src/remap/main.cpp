#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem.hpp>
#include "delly.h"
#include "somatic_filter.h"


int main(int argc, char **argv) {
  Config c;
  
  // Define generic options
  boost::program_options::options_description generic("Generic options");
  generic.add_options()
    ("help,?", "show help message")
    ("type,t", boost::program_options::value<std::string>(&c.svType)->default_value("ALL"), "SV analysis type (DEL, DUP, INV, TRA)")
    ("outfile,o", boost::program_options::value<boost::filesystem::path>(&c.outfile)->default_value("sv.vcf"), "SV output file")
    ("exclude,x", boost::program_options::value<boost::filesystem::path>(&c.exclude)->default_value(""), "file with chr to exclude")
    ;
  
  boost::program_options::options_description pem("PE options");
  pem.add_options()
    ("map-qual,q", boost::program_options::value<unsigned short>(&c.minMapQual)->default_value(0), "min. paired-end mapping quality")
    ("mad-cutoff,s", boost::program_options::value<unsigned short>(&c.madCutoff)->default_value(5), "insert size cutoff, median+s*MAD (deletions only)")
    ;
  
  boost::program_options::options_description breaks("SR options");
  breaks.add_options()
    ("genome,g", boost::program_options::value<boost::filesystem::path>(&c.genome), "genome fasta file")
    ("min-flank,m", boost::program_options::value<unsigned int>(&c.minimumFlankSize)->default_value(13), "minimum flanking sequence size")
    ;
  
  boost::program_options::options_description geno("Genotyping options");
  geno.add_options()
    ("vcfgeno,v", boost::program_options::value<boost::filesystem::path>(&c.vcffile)->default_value("site.vcf"), "input vcf file for genotyping only")
    ("geno-qual,u", boost::program_options::value<unsigned short>(&c.minGenoQual)->default_value(20), "min. mapping quality for genotyping")
    ;
  
  boost::program_options::options_description clus("Cluster options");
  clus.add_options()
    ("clus,z", boost::program_options::value<boost::filesystem::path>(&c.clusfile)->default_value(""), "cluster")
    ;
  
  // Define hidden options
  boost::program_options::options_description hidden("Hidden options");
  hidden.add_options()
    ("input-file", boost::program_options::value< std::vector<boost::filesystem::path> >(&c.files), "input file")
    ("epsilon,e", boost::program_options::value<float>(&c.epsilon)->default_value(0.1), "allowed epsilon deviation of PE vs. SR deletion")
    ("pe-fraction,c", boost::program_options::value<float>(&c.percentAbnormal)->default_value(0.0), "fixed fraction c of discordant PEs, for c=0 MAD cutoff is used")
    ("num-split,n", boost::program_options::value<unsigned int>(&c.minimumSplitRead)->default_value(2), "minimum number of splitted reads")
    ("flanking,f", boost::program_options::value<unsigned int>(&c.flankQuality)->default_value(80), "quality of the aligned flanking region")
    ("pruning,j", boost::program_options::value<unsigned int>(&c.graphPruning)->default_value(100), "PE graph pruning cutoff")
    ;
  
  boost::program_options::positional_options_description pos_args;
  pos_args.add("input-file", -1);
  
  // Set the visibility
  boost::program_options::options_description cmdline_options;
  cmdline_options.add(generic).add(pem).add(breaks).add(geno).add(clus).add(hidden);
  boost::program_options::options_description visible_options;
  visible_options.add(generic).add(pem).add(breaks).add(geno).add(clus);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).positional(pos_args).run(), vm);
  boost::program_options::notify(vm);
  
  
  // Check command line arguments
  if ((vm.count("help")) || (!vm.count("input-file"))) {
    //printTitle("DELLY");
    // TODO: fix
    std::cout << "Usage: " << argv[0] << " [OPTIONS] <sample1.sort.bam> <sample2.sort.bam> ..." << std::endl;
    std::cout << visible_options << "\n";
    return 1;
  }
  
  // Show cmd
  for(int i=0; i<argc; ++i) { std::cout << argv[i] << ' '; }
  std::cout << std::endl;
  
  
  vector<string> svTypes;
  if (c.svType == "ALL") {
    svTypes.push_back("DEL");
    svTypes.push_back("DUP");
    svTypes.push_back("INV");
    svTypes.push_back("TRA");
    // TODO: fix INS
    //svTypes.push_back("INS");
  } else {
    svTypes.push_back(c.svType);
  }
  
  // Run
  string realOutfile = c.outfile.string();

  for (size_t i = 0; i < svTypes.size(); ++i) {
    const string& svType = svTypes[i];
    string out1 = realOutfile + "." + svType + ".vcf";
    string out2 = realOutfile + "." + svType + ".somatic.vcf";
    
    std::cout << "Run delly for " << svType << std::endl << std::endl;
    c.svType = svType;
    c.outfile = out1;
    if (run_delly(c) != 0) {
      std::cerr << "An error has occurred" << std::endl;
      return 1;
    }
    
    std::cout << "Run somaticFilter for " << svType << std::endl << std::endl;
    if (run_somatic_filter(out1, out2, svType) != 0) {
      std::cerr << "An error has occurred" << std::endl;
      return 1;
    }
  }

  std::cout << "Merge VCF files" << std::endl;
  std::ofstream out(realOutfile.c_str());
  for (size_t i = 0; i < svTypes.size(); ++i) {
    const string& svType = svTypes[i];
    string out2 = realOutfile + "." + svType + ".somatic.vcf";
    std::ifstream file(out2.c_str());
    if (i > 0) { // skip the header
      string line;
      while (std::getline(file, line)) {
        if (!line.empty() && line[0] != '#') {
          out << line << std::endl;
          break;
        }
      }
    }
    out << file.rdbuf();
    file.close();
  }
  out.close();

  return 0;
}
