#include "vcf.h"
#include <vector>
#include <set>
#include <algorithm>
#include <sstream>
#include "IntervalTree.h"
#include "somatic_filter.h"

using namespace std;
using namespace vcf;

typedef pair<string, int> Entry;  // storing svId & score
typedef pair<int, int> Range;

struct Args {
  int minSize;
  int maxSize;
  float altAF;
  string svType;
} args;

template<typename T>
static T conv(const std::string& s) {
  T r;
  std::istringstream iss(s);
  iss >> r;
  return r;
}


static int gt_type(const string& GT) {
  if (GT.size() != 3) {
    printf("GT size is not 3\n"); return 1; // TODO: for now
  }
  if (GT[0] == GT[2])
    return (GT[0] == '0') ? 0 : 2;
  return 1;
}

static bool overlapValid(int s1, int e1, int s2, int e2, float reciprocalOverlap=0.8, int maxOffset=250) {
  int bpOffset = max(abs(s2-s1), abs(e2-e1));
  int overlapLen = min(e1, e2) - max(s1, s2);
  int lenA = e1 - s1;
  int lenB = e2 - s2;
  // Check for any overlap
  if ((e1 < s2) || (s1 > e2) || (lenA <= 0) || (lenB <= 0) || (overlapLen <= 0))
    return false;
  // Check reciprocal overlap and offset
  if (((float)overlapLen / max(lenA,lenB) < reciprocalOverlap) || (bpOffset > maxOffset))
    return false;
  return true;
}

int run_somatic_filter(const string& filename, const string& outFilename, const string& svType) {
  VariantCallFile variantFile;
  variantFile.open(filename);
  
  args.minSize = 500;
  args.maxSize = 500000000;
  args.altAF = 0.25;
  args.svType = svType; //"DEL";

  if (!variantFile.is_open()) {
    return 1;
  }
  
  string normalSampleName, tumorSampleName;
  set<string> validRecordID;
  map<string, map< Range,        Entry   > > sv;
  map<string, map< Range, vector<Entry> > > svDups;
  map<string, IntervalTree<Entry> > svTree;
  int traWindow = 5000;  // 5kb translocation window
  
  const vector<string>& sampleNames = variantFile.sampleNames;
  vector<string>::const_iterator sampleName = sampleNames.begin();
  for (; sampleName != sampleNames.end(); ++sampleName) {
    if (sampleName->find("normal") != string::npos) {
      normalSampleName = *sampleName;
    } else if (sampleName->find("tumor") != string::npos || sampleName->find("tumour") != string::npos) {
      tumorSampleName = *sampleName;
    }
  }
  if (normalSampleName == "" || tumorSampleName == "") {
    //fprintf(stderr, "need both samples\n");
    //return 1;
    printf("somatic_filter: Can't find two samples. Just copying the file now.\n");
    ifstream in(filename.c_str());
    ofstream out(outFilename.c_str());
    out << in.rdbuf();
    out.close();
    in.close();
    return 0;
  }


  {
  Variant record(variantFile);
  while (variantFile.getNextVariant(record)) {
    int svlen = conv<int>(record.info["SVLEN"]);
    if (svlen >= args.minSize && svlen <= args.maxSize) { // TODO: filter
      bool precise = false;
      if (record.infoFlags.find("PRECISE") != record.infoFlags.end()) { // FIXME: must remove infoFlags!!!
        precise = true;
      }

      vector<int> rcRef, rcAlt;
      {
        map<string, map<string, string> >::iterator sampleIt = record.samples.find(normalSampleName);
        if (sampleIt != record.samples.end()) {
          map<string, string>& sample = sampleIt->second;
          map<string, string>::const_iterator gt = sample.find("GT");
          if (gt != sample.end()) {
            const string& GT = gt->second;
            if (GT != "." && GT != "./." && gt_type(GT) == 0) {
              // if ((not precise) and (call['DV'] == 0)) or ((precise) and (call['RV']==0)):
              if ((!precise && sample["DV"] == "0") || (precise && sample["RV"] == "0")) {
                rcRef.push_back(conv<int>(sample["RC"]));
              }
            }
          }
        }
      }
      {
        map<string, map<string, string> >::iterator sampleIt = record.samples.find(tumorSampleName);
        if (sampleIt != record.samples.end()) {
          map<string, string>& sample = sampleIt->second;
          map<string, string>::const_iterator gt = sample.find("GT");
          if (gt != sample.end()) {
            const string& GT = gt->second;
            if (GT != "." && GT != "./." && gt_type(GT) != 0) {
              // if ((not precise) and (call['DV'] >= 2) and (float(call['DV'])/float(call['DV']+call['DR'])>=altAF)) or ((precise) and (call['RV'] >= 2) and (float(call['RV'])/float(call['RR'] + call['RV'])>=altAF)):
              int DV = conv<int>(sample["DV"]);
              int RV = conv<int>(sample["RV"]);
              if (
                (!precise && DV >= 2 && (float)DV/(DV+conv<int>(sample["DR"])) >= args.altAF) ||
                (precise && RV >= 2 && (float)RV/(RV+conv<int>(sample["RR"])) >= args.altAF)
              ) {
                rcAlt.push_back(conv<int>(sample["RC"]));
              }
            }
          }
        }
      }
      if (rcRef.size() > 0 && rcAlt.size() > 0) {
        //cout << record << endl;
        //printf("%d\n", record.position);
        
        validRecordID.insert(record.id);
        
        Entry entry = Entry(record.id, conv<int>(record.info["PE"]));
        
        if (args.svType == "TRA") {
          int POS = record.position;
          if ( sv[record.chrom].find(Range( POS - traWindow, POS + traWindow )) != sv[record.chrom].end() ) {
            svDups[record.chrom][Range( POS - traWindow, POS + traWindow )].push_back( entry );
          } else {
            sv[record.chrom][Range( POS - traWindow, POS + traWindow )] = entry;
          }
          int END = conv<int>(record.info["END"]);
          if ( sv[record.info["CHR2"]].find(Range( END - traWindow, END + traWindow )) != sv[record.info["CHR2"]].end() ) {
            svDups[record.info["CHR2"]][Range( END - traWindow, END + traWindow )].push_back( entry );
          } else {
            sv[record.info["CHR2"]][Range( END - traWindow, END + traWindow )] = entry;
          }
        } else {
          map<Range, Entry>::iterator i = sv[record.chrom].find(Range( record.position, conv<int>(record.info["END"]) ));
          if (i != sv[record.chrom].end()) {
            svDups[record.chrom][ Range( record.position, conv<int>(record.info["END"]) ) ].push_back( entry );
          } else {
            sv[record.chrom][ Range( record.position, conv<int>(record.info["END"]) ) ] = entry;
          }
        }
        
        /*
        validRecordID.add(record.ID)
        if not sv.has_key(record.CHROM):
            sv[record.CHROM] = banyan.SortedDict(key_type=(int, int), alg=banyan.RED_BLACK_TREE, updator=banyan.OverlappingIntervalsUpdator)
        if not sv.has_key(record.INFO['CHR2']):
            sv[record.INFO['CHR2']] = banyan.SortedDict(key_type=(int, int), alg=banyan.RED_BLACK_TREE, updator=banyan.OverlappingIntervalsUpdator)
        if (args.svType=='TRA'):
            traWindow=5000  # 5kb translocation window
            if (record.POS - traWindow, record.POS + traWindow) in sv[record.CHROM]:
                svDups[(record.CHROM, record.POS - traWindow, record.POS + traWindow)].append((record.ID, record.INFO['PE']))
            else:
                sv[record.CHROM][(record.POS - traWindow, record.POS + traWindow)] = (record.ID, record.INFO['PE'])
            if (record.INFO['END'] - traWindow, record.INFO['END'] + traWindow) in sv[record.INFO['CHR2']]:
                svDups[(record.INFO['CHR2'], record.INFO['END'] - traWindow, record.INFO['END'] + traWindow)].append((record.ID, record.INFO['PE']))
            else:
                sv[record.INFO['CHR2']][(record.INFO['END'] - traWindow, record.INFO['END'] + traWindow)] = (record.ID, record.INFO['PE'])
        else:
            if (record.POS, record.INFO['END']) in sv[record.CHROM]:
                svDups[(record.CHROM, record.POS, record.INFO['END'])].append((record.ID, record.INFO['PE']))
            else:
                sv[record.CHROM][(record.POS, record.INFO['END'])] = (record.ID, record.INFO['PE'])
        */
      }
      

    }
  }
  }
  
  
    
  map<string, map<Range, Entry> >::const_iterator svIt = sv.begin();
  for (; svIt != sv.end(); ++svIt) {
    vector<Interval<Entry> > intervals;

    const map<Range, Entry>& m = svIt->second;
    map<Range, Entry>::const_iterator mIt = m.begin();
    for (; mIt != m.end(); ++mIt) {
      intervals.push_back(Interval<Entry>(mIt->first.first, mIt->first.second, mIt->second));
    }
    
    svTree[svIt->first] = IntervalTree<Entry>(intervals);
  }
  
  
  
  ofstream out(outFilename.c_str());
  if (!out.is_open()) {
    cerr << "Can't write the output" << endl;
    return 1;
  }
  variantFile.addHeaderInfoLine("##INFO=<ID=SOMATIC,Number=0,Type=Flag,Description=\"Somatic structural variant.\">");
  out << variantFile.header << endl;
  
  
  {
  VariantCallFile variantFile;
  variantFile.open(filename);
  Variant record(variantFile);
  
  while (variantFile.getNextVariant(record)) {
    if (validRecordID.find(record.id) == validRecordID.end())
      continue;

    // Judge whether overlapping calls are better    
    bool foundBetterHit = false;
    if (args.svType == "TRA") {
      string chrNames[2] = {record.chrom, record.info["CHR2"]};
      int starts[2] = {(int)record.position - traWindow, conv<int>(record.info["END"]) - traWindow};
      int ends[2] = {(int)record.position + traWindow, conv<int>(record.info["END"]) + traWindow};
      for (int k = 0; k < 2; ++k) {
        string chrName = chrNames[k];
        int start = starts[k], end = ends[k];
        
        vector<Interval<Entry> > results;
        svTree[chrName].findOverlapping(start, end, results);
        for (vector<Interval<Entry> >::const_iterator i = results.begin(); i != results.end(); ++i) {
          int cStart = i->start, cEnd = i->stop;

          // TODO: be careful!!! check the results & the code!!
          vector<Entry> entries = svDups[chrName][Range(cStart, cEnd)];
          entries.push_back(sv[chrName][Range(cStart, cEnd)]);
          for (size_t j = 0; j < entries.size(); ++j) {
            const string& cSvID = entries[j].first;
            int cScore = entries[j].second;
            if (record.id != cSvID) {
              int PE = conv<int>(record.info["PE"]);
              if ((cScore > PE) || ((cScore == PE) && (cSvID < record.id))) {
                foundBetterHit = true;
                break;
              }
            }
          }
          if (foundBetterHit)
            break;
        }
      }
      /*
      for (chrName, start, end) in [ (record.CHROM, record.POS - traWindow, record.POS + traWindow), (record.INFO['CHR2'], record.INFO['END'] - traWindow, record.INFO['END'] + traWindow) ]:
        for cStart, cEnd in sv[chrName].overlap((start, end)):
            for cSvID, cScore in svDups[(chrName, cStart, cEnd)] + [sv[chrName][(cStart, cEnd)]]:
                if (record.ID != cSvID):
                    if (cScore > record.INFO['PE']) or ((cScore == record.INFO['PE']) and (cSvID < record.ID)):
                        foundBetterHit = True
                        break
            if (foundBetterHit):
                break
      */
    } else {
      vector<Interval<Entry> > results;
      svTree[record.chrom].findOverlapping(record.position, conv<int>(record.info["END"]), results);
      for (vector<Interval<Entry> >::const_iterator i = results.begin(); i != results.end(); ++i) {
        int cStart = i->start, cEnd = i->stop;
        
        vector<Entry> entries = svDups[record.chrom][Range(cStart, cEnd)];
        entries.push_back(sv[record.chrom][Range(cStart, cEnd)]);
        for (size_t j = 0; j < entries.size(); ++j) {
          const string& cSvID = entries[j].first;
          int cScore = entries[j].second;
          if ((record.id != cSvID) && (overlapValid(record.position, conv<int>(record.info["END"]), cStart, cEnd))) {
            int PE = conv<int>(record.info["PE"]);
            if ((cScore > PE) || ((cScore == PE) && (cSvID < record.id))) {
              foundBetterHit = true;
              break;
            }
          }
        }
        if (foundBetterHit)
          break;
      }
    }
    if (!foundBetterHit) {
      record.infoFlags["SOMATIC"] = true;
      out << record << endl;
    }
  }
  }
  
  out.close();

  return 0;
}
