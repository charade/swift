#ifndef SOMATIC_FILTER_H
#define SOMATIC_FILTER_H

#include <string>

int run_somatic_filter(const std::string& filename, const std::string& outFilename, const std::string& svType);

#endif
