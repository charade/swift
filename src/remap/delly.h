#ifndef DELLY_H
#define DELLY_H

// Config arguments
struct DellyConfig {
	unsigned short minMapQual;
	unsigned short minGenoQual;
	unsigned short madCutoff;
	unsigned int minimumFlankSize;
	unsigned int minimumSplitRead;
	unsigned int flankQuality;
	unsigned int graphPruning;
	float epsilon;
	float percentAbnormal;
	std::string svType;
	boost::filesystem::path outfile;
	boost::filesystem::path vcffile;
	boost::filesystem::path clusfile;
	boost::filesystem::path genome;
	boost::filesystem::path exclude;
	std::vector<boost::filesystem::path> files;
};

class sv;
int run_delly(DellyConfig const& c, const std::vector<SV>& swiftSVs);

#endif
