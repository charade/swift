#include "vcf.h"
#include <sstream>

namespace vcf {

  // split.h + split.cpp

  // from Marius, http://stackoverflow.com/a/1493195/238609
  template < class ContainerT >
  static void tokenize(const std::string& str, ContainerT& tokens,
                const std::string& delimiters = " ", const bool trimEmpty = false)
  {
    std::string::size_type pos, lastPos = 0;
    while(true) {
      pos = str.find_first_of(delimiters, lastPos);
      if (pos == std::string::npos) {
        pos = str.length();
        if (pos != lastPos || !trimEmpty) {
          tokens.push_back(typename ContainerT::value_type(str.data()+lastPos, (typename ContainerT::value_type::size_type)pos-lastPos));
        }
        break;
      } else {
        if(pos != lastPos || !trimEmpty) {
          tokens.push_back(typename ContainerT::value_type(str.data()+lastPos, (typename ContainerT::value_type::size_type)pos-lastPos));
        }
      }
      lastPos = pos + 1;
    }
  }

  static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::string delims = std::string(1, delim);
    tokenize(s, elems, delims);
    return elems;
  }

  static std::vector<std::string> split(const std::string &s, char delim) {
      std::vector<std::string> elems;
      return split(s, delim, elems);
  }

  static std::vector<std::string> &split(const std::string &s, const std::string& delims, std::vector<std::string> &elems) {
      tokenize(s, elems, delims);
      return elems;
  }

  static std::vector<std::string> split(const std::string &s, const std::string& delims) {
      std::vector<std::string> elems;
      return split(s, delims, elems);
  }

  // join.h

  template<class S, class T>
  static std::string join(std::vector<T>& elems, S& delim) {
    std::stringstream ss;
    typename std::vector<T>::iterator e = elems.begin();
    ss << *e++;
    for (; e != elems.end(); ++e) {
        ss << delim << *e;
    }
    return ss.str();
  }
  
  // convert.h
  
  // converts the string into the specified type, setting r to the converted
  // value and returning true/false on success or failure
  template<typename T>
  static bool convert(const std::string& s, T& r) {
      std::istringstream iss(s);
      iss >> r;
      return iss.eof() ? true : false;
  }

  template<typename T>
  static std::string convert(const T& r) {
      std::ostringstream oss;
      oss << r;
      return oss.str();
  }

  // 
  
  void Variant::parse(string& line, bool parseSamples) {
    
    // clean up potentially variable data structures
    info.clear();
    infoFlags.clear();
    format.clear();
    
    // #CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT [SAMPLE1 .. SAMPLEN]
    vector<string> fields = split(line, '\t');
    if (fields.size() < 7) {
      cerr << "broken VCF record (less than 7 fields)" << endl
      << line << endl;
      exit(1);
    }
    
    chrom = fields.at(0);
    char* end; // dummy variable for strtoll
    position = strtoll(fields.at(1).c_str(), &end, 10);
    id = fields.at(2);
    ref = fields.at(3);
    alt = fields.at(4);    
    quality = fields.at(5);
    filter = fields.at(6);
    if (fields.size() > 7) {
      vector<string> infofields = split(fields.at(7), ';');
      for (vector<string>::iterator f = infofields.begin(); f != infofields.end(); ++f) {
        if (*f == ".") {
          continue;
        }
        vector<string> kv = split(*f, '=');
        if (kv.size() == 2) {
          info[kv.at(0)] = kv.at(1);
        } else if (kv.size() == 1) {
          infoFlags[kv.at(0)] = true;
        }
      }
    }
    // check if we have samples specified
    // and that we are supposed to parse them
    if (parseSamples && fields.size() > 8) {
      format = split(fields.at(8), ':');
      // if the format changed, we have to rebuild the samples
      if (fields.at(8) != lastFormat) {
        samples.clear();
        lastFormat = fields.at(8);
      }
      vector<string>::iterator sampleName = sampleNames.begin();
      vector<string>::iterator sample = fields.begin() + 9;
      for (; sample != fields.end() && sampleName != sampleNames.end(); ++sample, ++sampleName) {
        string& name = *sampleName;
        if (*sample == "." || *sample == "./.") {
          samples.erase(name);
          continue;
        }
        vector<string> samplefields = split(*sample, ':');
        vector<string>::iterator i = samplefields.begin();
        if (samplefields.size() != format.size()) {
          // ignore this case... malformed (or 'null') sample specs are caught above
          // /*
          // cerr << "inconsistent number of fields for sample " << name << endl
          //      << "format is " << join(format, ":") << endl
          //      << "sample is " << *sample << endl;
          // exit(1);
          // *
        } else {
          for (vector<string>::iterator f = format.begin(); f != format.end(); ++f) {
            //samples[name][*f] = split(*i, ',');
            samples[name][*f] = *i;
            ++i;
            //cout << *f << endl;
          }
        }
      }
      if (sampleName != sampleNames.end()) {
        cerr << "error: more sample names in header than sample fields" << endl;
        cerr << "samples: " << join(sampleNames, " ") << endl;
        cerr << "line: " << line << endl;
        exit(1);
      }
      if (sample != fields.end()) {
        cerr << "error: more sample fields than samples listed in header" << endl;
        cerr << "samples: " << join(sampleNames, " ") << endl;
        cerr << "line: " << line << endl;
        exit(1);
      }
    } else if (!parseSamples) {
      originalLine = line;
    }
    
    //return true; // we should be catching exceptions...
  }
  
  void Variant::setVariantCallFile(VariantCallFile& v) {
    sampleNames = v.sampleNames;
    outputSampleNames = v.sampleNames;
    vcf = &v;
  }
  
  void Variant::setVariantCallFile(VariantCallFile* v) {
    sampleNames = v->sampleNames;
    outputSampleNames = v->sampleNames;
    vcf = v;
  }
  
  ostream& operator<<(ostream& out, VariantFieldType type) {
    switch (type) {
      case FIELD_INTEGER:
        out << "integer";
        break;
      case FIELD_FLOAT:
        out << "float";
        break;
      case FIELD_BOOL:
        out << "bool";
        break;
      case FIELD_STRING:
        out << "string";
        break;
      default:
        out << "unknown";
        break;
    }
    return out;
  }
  
  VariantFieldType typeStrToVariantFieldType(string& typeStr) {
    if (typeStr == "Integer") {
      return FIELD_INTEGER;
    } else if (typeStr == "Float") {
      return FIELD_FLOAT;
    } else if (typeStr == "Flag") {
      return FIELD_BOOL;
    } else if (typeStr == "String") {
      return FIELD_STRING;
    } else {
      return FIELD_UNKNOWN;
    }
  }
  
  VariantFieldType Variant::infoType(string& key) {
    map<string, VariantFieldType>::iterator s = vcf->infoTypes.find(key);
    if (s == vcf->infoTypes.end()) {
      if (key == "QUAL") { // hack to use QUAL as an "info" field
        return FIELD_INTEGER;
      }
      cerr << "no info field " << key << endl;
      exit(1);
    } else {
      return s->second;
    }
  }
  
    VariantFieldType Variant::formatType(string& key) {
        map<string, VariantFieldType>::iterator s = vcf->formatTypes.find(key);
        if (s == vcf->formatTypes.end()) {
            cerr << "no format field " << key << endl;
            exit(1);
        } else {
            return s->second;
        }
    }
  
    bool Variant::getInfoValueBool(string& key, int index) {
        map<string, VariantFieldType>::iterator s = vcf->infoTypes.find(key);
        if (s == vcf->infoTypes.end()) {
            cerr << "no info field " << key << endl;
            exit(1);
        } else {
            int count = vcf->infoCounts[key];
            // XXX TODO, fix for Genotype variants...
            if (count != ALLELE_NUMBER) {
                index = 0;
            }
            if (index == INDEX_NONE) {
                if (count != 1) {
                    cerr << "no field index supplied and field count != 1" << endl;
                    exit(1);
                } else {
                    index = 0;
                }
            }
            VariantFieldType type = s->second;
            if (type == FIELD_BOOL) {
                map<string, bool>::iterator b = infoFlags.find(key);
                if (b == infoFlags.end())
                    return false;
                else
                    return true;
            } else {
                cerr << "not flag type " << key << endl;
            }
        }
        return false;
    }
  
    string Variant::getInfoValueString(string& key, int index) {
        map<string, VariantFieldType>::iterator s = vcf->infoTypes.find(key);
        if (s == vcf->infoTypes.end()) {
            cerr << "no info field " << key << endl;
            exit(1);
        } else {
            int count = vcf->infoCounts[key];
            // XXX TODO, fix for Genotype variants...
            if (count != ALLELE_NUMBER) {
                index = 0;
            }
            if (index == INDEX_NONE) {
                if (count != 1) {
                    cerr << "no field index supplied and field count != 1" << endl;
                    exit(1);
                } else {
                    index = 0;
                }
            }
            VariantFieldType type = s->second;
            if (type == FIELD_STRING) {
                map<string, string>::iterator b = info.find(key);
                if (b == info.end())
                    return "";
                return b->second;
            } else {
                cerr << "not string type " << key << endl;
                return "";
            }
        }
        return "";
    }
  
    double Variant::getInfoValueFloat(string& key, int index) {
        map<string, VariantFieldType>::iterator s = vcf->infoTypes.find(key);
        if (s == vcf->infoTypes.end()) {
            cerr << "no info field " << key << endl;
            exit(1);
        } else {
            int count = vcf->infoCounts[key];
            // XXX TODO, fix for Genotype variants...
            if (count != ALLELE_NUMBER) {
                index = 0;
            }
            if (index == INDEX_NONE) {
                if (count != 1) {
                    cerr << "no field index supplied and field count != 1" << endl;
                    exit(1);
                } else {
                    index = 0;
                }
            }
            VariantFieldType type = s->second;
            if (type == FIELD_FLOAT || type == FIELD_INTEGER) {
                map<string, string>::iterator b = info.find(key);
                if (b == info.end())
                    return false;
                double r;
                if (!convert(b->second, r)) {
                    cerr << "could not convert field " << key << "=" << b->second.at(index) << " to " << type << endl;
                    exit(1);
                }
                return r;
            } else {
                cerr << "unsupported type for variant record " << type << endl;
                exit(1);
            }
        }
        return 0.0;
    }
  
    int Variant::getNumSamples(void) {
        return sampleNames.size();
    }
  
    bool Variant::getSampleValueBool(string& key, string& sample, int index) {
        map<string, VariantFieldType>::iterator s = vcf->formatTypes.find(key);
        if (s == vcf->infoTypes.end()) {
            cerr << "no info field " << key << endl;
            exit(1);
        } else {
            int count = vcf->formatCounts[key];
            // XXX TODO, fix for Genotype variants...
            if (count != ALLELE_NUMBER) {
                index = 0;
            }
            if (index == INDEX_NONE) {
                if (count != 1) {
                    cerr << "no field index supplied and field count != 1" << endl;
                    exit(1);
                } else {
                    index = 0;
                }
            }
            VariantFieldType type = s->second;
            map<string, string >& sampleData = samples[sample];
            if (type == FIELD_BOOL) {
                map<string, string >::iterator b = sampleData.find(key);
                if (b == sampleData.end())
                    return false;
                else
                    return true;
            } else {
                cerr << "not bool type " << key << endl;
            }
        }
        return false;
    }
  
    string Variant::getSampleValueString(string& key, string& sample, int index) {
        map<string, VariantFieldType>::iterator s = vcf->formatTypes.find(key);
        if (s == vcf->infoTypes.end()) {
            cerr << "no info field " << key << endl;
            exit(1);
        } else {
            int count = vcf->formatCounts[key];
            // XXX TODO, fix for Genotype variants...
            if (count != ALLELE_NUMBER) {
                index = 0;
            }
            if (index == INDEX_NONE) {
                if (count != 1) {
                    cerr << "no field index supplied and field count != 1" << endl;
                    exit(1);
                } else {
                    index = 0;
                }
            }
            VariantFieldType type = s->second;
            map<string, string >& sampleData = samples[sample];
            if (type == FIELD_STRING) {
                map<string, string >::iterator b = sampleData.find(key);
                if (b == sampleData.end()) {
                    return "";
                } else {
                    return b->second;
                }
            } else {
                cerr << "not string type " << key << endl;
            }
        }
        return "";
    }
  
    double Variant::getSampleValueFloat(string& key, string& sample, int index) {
        map<string, VariantFieldType>::iterator s = vcf->formatTypes.find(key);
        if (s == vcf->infoTypes.end()) {
            cerr << "no info field " << key << endl;
            exit(1);
        } else {
            // XXX TODO wrap this with a function call
            int count = vcf->formatCounts[key];
            // XXX TODO, fix for Genotype variants...
            if (count != ALLELE_NUMBER) {
                index = 0;
            }
            if (index == INDEX_NONE) {
                if (count != 1) {
                    cerr << "no field index supplied and field count != 1" << endl;
                    exit(1);
                } else {
                    index = 0;
                }
            }
            VariantFieldType type = s->second;
            map<string, string >& sampleData = samples[sample];
            if (type == FIELD_FLOAT || type == FIELD_INTEGER) {
                map<string, string >::iterator b = sampleData.find(key);
                if (b == sampleData.end())
                    return false;
                double r;
                if (!convert(b->second, r)) {
                    cerr << "could not convert field " << key << "=" << b->second.at(index) << " to " << type << endl;
                    exit(1);
                }
                return r;
            } else {
                cerr << "unsupported type for sample " << type << endl;
            }
        }
        return 0.0;
    }
  
    bool Variant::getValueBool(string& key, string& sample, int index) {
        if (sample.empty()) { // an empty sample name means
            return getInfoValueBool(key, index);
        } else {
            return getSampleValueBool(key, sample, index);
        }
    }
  
    double Variant::getValueFloat(string& key, string& sample, int index) {
        if (sample.empty()) { // an empty sample name means
            return getInfoValueFloat(key, index);
        } else {
            return getSampleValueFloat(key, sample, index);
        }
    }
  
    string Variant::getValueString(string& key, string& sample, int index) {
        if (sample.empty()) { // an empty sample name means
            return getInfoValueString(key, index);
        } else {
            return getSampleValueString(key, sample, index);
        }
    }

    void Variant::addFilter(string& tag) {
        if (filter == "" || filter == ".")
            filter = tag;
        else
            filter += "," + tag;
    }
  
    void Variant::addFormatField(string& key) {
        bool hasTag = false;
        for (vector<string>::iterator t = format.begin(); t != format.end(); ++t) {
            if (*t == key) {
                hasTag = true;
                break;
            }
        }
        if (!hasTag) {
            format.push_back(key);
        }
    }
  
    ostream& operator<<(ostream& out, Variant& var) {
        // ensure there are no empty fields
        if (var.chrom.empty()) var.chrom = ".";
        if (var.id.empty()) var.id = ".";
        if (var.ref.empty()) var.ref = ".";
        if (var.alt.empty()) var.alt = ".";
        if (var.filter.empty()) var.filter = ".";
    
        out << var.chrom << "\t"
    << var.position << "\t"
    << var.id << "\t"
    << var.ref << "\t"
    << var.alt << "\t"
    << var.quality << "\t"
    << var.filter << "\t";
        if (var.info.empty() && var.infoFlags.empty()) {
            out << ".";
        } else {
            for (map<string, string>::iterator i = var.info.begin(); i != var.info.end(); ++i) {
                if (!i->second.empty()) {
                    out << ((i == var.info.begin()) ? "" : ";") << i->first << "=" << i->second;
                }
            }
            for (map<string, bool>::iterator i = var.infoFlags.begin(); i != var.infoFlags.end(); ++i) {
                if (i == var.infoFlags.end()) {
                    out << "";
                } else if (i == var.infoFlags.begin() && var.info.empty()) {
                    out << "";
                } else {
                    out << ";";
                }
                out << i->first;
            }
        }
        if (!var.format.empty()) {
            out << "\t";
            for (vector<string>::iterator f = var.format.begin(); f != var.format.end(); ++f) {
                out << ((f == var.format.begin()) ? "" : ":") << *f;
            }
            for (vector<string>::iterator s = var.outputSampleNames.begin(); s != var.outputSampleNames.end(); ++s) {
                out << "\t";
                map<string, map<string, string> >::iterator sampleItr = var.samples.find(*s);
                if (sampleItr == var.samples.end()) {
                    out << ".";
                } else {
                    map<string, string>& sample = sampleItr->second;
                    if (sample.size() == 0) {
                        out << ".";
                    } else {
                        for (vector<string>::iterator f = var.format.begin(); f != var.format.end(); ++f) {
                            map<string, string>::iterator g = sample.find(*f);
                            out << ((f == var.format.begin()) ? "" : ":");
                            if (g != sample.end() && !g->second.empty()) {
                                out << g->second;
                            } else {
                                out << ".";
                            }
                        }
                    }
                }
            }
        }
        return out;
    }
  
    void Variant::setOutputSampleNames(vector<string>& samplesToOutput) {
        outputSampleNames = samplesToOutput;
    }
  
  
  
  
  
  /*
   bool VariantCallFile::openVCF(string& filename) {
   file.open(filename.c_str(), ifstream::in);
   if (!file.is_open()) {
   cerr << "could not open " << filename << endl;
   return false;
   } else {
   return parseHeader();
   }
   }
   
   bool VariantCallFile::openVCF(ifstream& stream) {
   file = stream;
   if (!file.is_open()) {
   cerr << "provided file is not open" << endl;
   return false;
   } else {
   return parseHeader();
   }
   }
   */
  
  void VariantCallFile::updateSamples(vector<string>& newSamples) {
    sampleNames = newSamples;
    // regenerate the last line of the header
    vector<string> headerLines = split(header, '\n');
    vector<string> colnames = split(headerLines.at(headerLines.size() - 1), '\t'); // get the last, update the samples
    vector<string> newcolnames;
    newcolnames.resize(9 + sampleNames.size());
    copy(colnames.begin(), colnames.begin() + 9, newcolnames.begin());
    copy(sampleNames.begin(), sampleNames.end(), newcolnames.begin() + 9);
    headerLines.at(headerLines.size() - 1) = join(newcolnames, "\t");
    header = join(headerLines, "\n");
  }
  
  // non-destructive version of above
  string VariantCallFile::headerWithSampleNames(vector<string>& newSamples) {
    // regenerate the last line of the header
    if (newSamples.empty()) return header;
    vector<string> headerLines = split(header, '\n');
    vector<string> colnames = split(headerLines.at(headerLines.size() - 1), '\t'); // get the last, update the samples
    vector<string> newcolnames;
    newcolnames.resize(9 + newSamples.size());
    copy(colnames.begin(), colnames.begin() + 9, newcolnames.begin());
    copy(newSamples.begin(), newSamples.end(), newcolnames.begin() + 9);
    headerLines.at(headerLines.size() - 1) = join(newcolnames, "\t");
    return join(headerLines, "\n");
  }
  
  // TODO cleanup, store header lines instead of bulk header
  void VariantCallFile::addHeaderLine(string line) {
    vector<string> headerLines = split(header, '\n');
    headerLines.insert(headerLines.end() - 1, line);
    header = join(unique(headerLines), "\n");
  }

  void VariantCallFile::addHeaderInfoLine(string line) {
    vector<string> headerLines = split(header, '\n');
    size_t i = headerLines.size() - 1;
    for (size_t j = i; j > 0; --j) {
      if (headerLines[j].length() >= 7 && headerLines[j].compare(0, 7, "##INFO=") == 0) {
        i = j + 1;
        break;
      }
    }
    headerLines.insert(headerLines.begin() + i, line);
    header = join(unique(headerLines), "\n");
  }

  // helper to addHeaderLine
  vector<string>& unique(vector<string>& strings) {
    set<string> uniq;
    vector<string> res;
    for (vector<string>::const_iterator s = strings.begin(); s != strings.end(); ++s) {
      if (uniq.find(*s) == uniq.end()) {
        res.push_back(*s);
        uniq.insert(*s);
      }
    }
    strings = res;
    return strings;
  }

  vector<string> VariantCallFile::infoIds(void) {
    vector<string> tags;
    vector<string> headerLines = split(header, '\n');
    for (vector<string>::iterator s = headerLines.begin(); s != headerLines.end(); ++s) {
      string& line = *s;
      if (line.find("##INFO") == 0) {
        size_t pos = line.find("ID=");
        if (pos != string::npos) {
          pos += 3;
          size_t tagend = line.find(",", pos);
          if (tagend != string::npos) {
            tags.push_back(line.substr(pos, tagend - pos));
          }
        }
      }
    }
    return tags;
  }
  
  vector<string> VariantCallFile::formatIds(void) {
    vector<string> tags;
    vector<string> headerLines = split(header, '\n');
    for (vector<string>::iterator s = headerLines.begin(); s != headerLines.end(); ++s) {
      string& line = *s;
      if (line.find("##FORMAT") == 0) {
        size_t pos = line.find("ID=");
        if (pos != string::npos) {
          pos += 3;
          size_t tagend = line.find(",", pos);
          if (tagend != string::npos) {
            tags.push_back(line.substr(pos, tagend - pos));
          }
        }
      }
    }
    return tags;
  }
  
  void VariantCallFile::removeInfoHeaderLine(string tag) {
    vector<string> headerLines = split(header, '\n');
    vector<string> newHeader;
    string id = "ID=" + tag + ",";
    for (vector<string>::iterator s = headerLines.begin(); s != headerLines.end(); ++s) {
      string& line = *s;
      if (line.find("##INFO") == 0) {
        if (line.find(id) == string::npos) {
          newHeader.push_back(line);
        }
      } else {
        newHeader.push_back(line);
      }
    }
    header = join(newHeader, "\n");
  }
  
  void VariantCallFile::removeGenoHeaderLine(string tag) {
    vector<string> headerLines = split(header, '\n');
    vector<string> newHeader;
    string id = "ID=" + tag + ",";
    for (vector<string>::iterator s = headerLines.begin(); s != headerLines.end(); ++s) {
      string& headerLine = *s;
      if (headerLine.find("##FORMAT") == 0) {
        if (headerLine.find(id) == string::npos) {
          newHeader.push_back(headerLine);
        }
      } else {
        newHeader.push_back(headerLine);
      }
    }
    header = join(newHeader, "\n");
  }
  
  bool VariantCallFile::parseHeader(void) {
    
    string headerStr = "";
    
    while (std::getline(*file, line)) {
      if (line.substr(0,1) == "#") {
        headerStr += line + '\n';
      } else {
        // done with header
        if (headerStr.empty()) {
          cerr << "error: no VCF header" << endl;
          exit(1);
        }
        firstRecord = true;
        break;
      }
    }
  
    return parseHeader(headerStr);
    
  }
  
  bool VariantCallFile::parseHeader(string& hs) {
    
    if (hs.substr(hs.size() - 1, 1) == "\n") {
      hs.erase(hs.size() - 1, 1); // remove trailing newline
    }
    header = hs; // stores the header in the object instance
    
    vector<string> headerLines = split(header, "\n");
    for (vector<string>::iterator h = headerLines.begin(); h != headerLines.end(); ++h) {
      string headerLine = *h;
      if (headerLine.substr(0,2) == "##") {
        // meta-information headerLines
        // TODO parse into map from info/format key to type
        // ##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">
        // ##FORMAT=<ID=CB,Number=1,Type=String,Description="Called by S(Sanger), M(UMich), B(BI)">
        size_t found = headerLine.find_first_of("=");
        string entryType = headerLine.substr(2, found - 2);
        // handle reference here, no "<" and ">" given
                //} else if (entryType == "reference") {
        size_t dataStart = headerLine.find_first_of("<");
        size_t dataEnd = headerLine.find_first_of(">");
        if (dataStart != string::npos && dataEnd != string::npos) {
          string entryData = headerLine.substr(dataStart + 1, dataEnd - dataStart - 1);
          // XXX bad; this will break if anyone ever moves the order
          // of the fields around to include a "long form" string
          // including either a = or , in the first or second field
          if (entryType == "INFO" || entryType == "FORMAT") {
            vector<string> fields = split(entryData, "=,");
            if (fields[0] != "ID") {
              cerr << "header parse error at:" << endl
              << "fields[0] != \"ID\"" << endl
              << headerLine << endl;
              exit(1);
            }
            string id = fields[1];
            if (fields[2] != "Number") {
              cerr << "header parse error at:" << endl
              << "fields[2] != \"Number\"" << endl
              << headerLine << endl;
              exit(1);
            }
            int number;
            string numberstr = fields[3].c_str();
            // XXX TODO VCF has variable numbers of fields...
            if (numberstr == "A") {
              number = ALLELE_NUMBER;
            } else if (numberstr == "G") {
              number = GENOTYPE_NUMBER;
            } else if (numberstr == ".") {
              number = 1;
            } else {
              convert(numberstr, number);
            }
            if (fields[4] != "Type") {
              cerr << "header parse error at:" << endl
              << "fields[4] != \"Type\"" << endl
              << headerLine << endl;
              exit(1);
            }
            VariantFieldType type = typeStrToVariantFieldType(fields[5]);
            if (entryType == "INFO") {
              infoCounts[id] = number;
              infoTypes[id] = type;
              //cerr << id << " == " << type << endl;
            } else if (entryType == "FORMAT") {
              //cout << "found format field " << id << " with type " << type << endl;
              formatCounts[id] = number;
              formatTypes[id] = type;
            }
          }
        }
      } else if (headerLine.substr(0,1) == "#") {
        // field name headerLine
        vector<string> fields = split(headerLine, '\t');
        if (fields.size() > 8) {
          sampleNames.resize(fields.size() - 9);
          copy(fields.begin() + 9, fields.end(), sampleNames.begin());
        }
      }
    }
    
    return true;
  }
  
  bool VariantCallFile::getNextVariant(Variant& var) {
    if (firstRecord && !justSetRegion) {
      if (!line.empty()) {
        var.parse(line, parseSamples);
        firstRecord = false;
        _done = false;
        return true;
      } else {
        return false;
      }
    }
    {
      if (std::getline(*file, line)) {
        var.parse(line, parseSamples);
        _done = false;
        return true;
      } else {
        _done = true;
        return false;
      }
    }
  }
  
  // union of lines in headers of input files
  string unionInfoHeaderLines(string& s1, string& s2) {
    vector<string> lines1 = split(s1, "\n");
    vector<string> lines2 = split(s2, "\n");
    vector<string> result;
    set<string> l2;
    string lastHeaderLine; // this one needs to be at the end
    for (vector<string>::iterator s = lines2.begin(); s != lines2.end(); ++s) {
      if (s->substr(0,6) == "##INFO") {
        l2.insert(*s);
      }
    }
    for (vector<string>::iterator s = lines1.begin(); s != lines1.end(); ++s) {
      if (l2.count(*s)) {
        l2.erase(*s);
      }
      if (s->substr(0,6) == "#CHROM") {
        lastHeaderLine = *s;
      } else {
        result.push_back(*s);
      }
    }
    for (set<string>::iterator s = l2.begin(); s != l2.end(); ++s) {
      result.push_back(*s);
    }
    if (lastHeaderLine.empty()) {
      cerr << "could not find CHROM POS ... header line" << endl;
      exit(1);
    }
    result.push_back(lastHeaderLine);
    return join(result, "\n");
  }
  
  /*
  bool Variant::isPhased(void) {
    for (map<string, map<string, vector<string> > >::iterator s = samples.begin(); s != samples.end(); ++s) {
      map<string, vector<string> >& sample = s->second;
      map<string, vector<string> >::iterator g = sample.find("GT");
      if (g != sample.end()) {
        string gt = g->second.front();
        if (gt.size() > 1 && gt.find("|") == string::npos) {
          return false;
        }
      }
    }
    return true;
  }
  */
  
  long Variant::zeroBasedPosition(void) {
    return position - 1;
  }
  
} // end namespace vcf
